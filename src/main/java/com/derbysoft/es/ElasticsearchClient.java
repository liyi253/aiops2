package com.derbysoft.es;
 
  
//import com.derbysoft.dlqs.common.constant.CommonConstant;
//import com.derbysoft.dlqs.common.constant.HttpConstant;
import org.apache.http.HeaderElement;
 
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
 
import javax.net.ssl.SSLContext;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
 
@Component
public class ElasticsearchClient implements DisposableBean {
     private static final Logger LOGGER = LoggerFactory.getLogger(ElasticsearchClient.class);
 
     private RestHighLevelClient restHighLevelClient;
 
     @Value("${elasticsearch.cluster.name}")
     private String clusterName;
 
     @Value("${elasticsearch.host}")
     private String host;
 
     @Value("${elasticsearch.request.name}")
     private String requestName;
 
     @Value("${elasticsearch.request.pwd}")
     private String requestPwd;
 
     @Value("${elasticsearch.request.schema}")
     private String requestSchema;
 
 
     private List<HttpHost> getHttpHost() {
         List<HttpHost> httpList = new ArrayList<HttpHost>();
 
         Arrays.stream(host.split(",")).forEach(item -> {
             String[] address = item.split(":");
             if (address.length >= 1) {
                 httpList.add(new HttpHost(address[0], Integer.valueOf(address[1]), requestSchema));
             }
         });
         return httpList;
     }
 
     private CredentialsProvider getCredentialsProvider() {
         CredentialsProvider credentialsProvider =
                 new BasicCredentialsProvider();
         credentialsProvider.setCredentials(AuthScope.ANY,
                 new UsernamePasswordCredentials(requestName, requestPwd));
         return credentialsProvider;
     }
 
     private SSLContext getSSLContext() {
         SSLContextBuilder sslBuilder = null;
         try {
             sslBuilder = SSLContexts.custom()
                     .loadTrustMaterial(null, new TrustStrategy() {
                         @Override
                         public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                             return true;
                         }
                     });
             return sslBuilder.build();
         } catch (Exception e) {
             e.printStackTrace();
         }
         return null;
     }
 
     private RestClientBuilder setHttpClient(RestClientBuilder builder) {
         builder.setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
             @Override
             public HttpAsyncClientBuilder customizeHttpClient(
                     HttpAsyncClientBuilder httpClientBuilder) {
                 return httpClientBuilder
                         .setDefaultCredentialsProvider(getCredentialsProvider())
                         .setSSLContext(getSSLContext())
                         .setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {
                             @Override
                             public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
                                 if (response == null) {
                                     throw new IllegalArgumentException("HTTP response may not be null");
                                 }
                                 HeaderElementIterator it = new BasicHeaderElementIterator(
                                         response.headerIterator(HTTP.CONN_KEEP_ALIVE));
                                 while (it.hasNext()) {
                                     HeaderElement he = it.nextElement();
                                     String param = he.getName();
                                     String value = he.getValue();
                                     if (value != null && param.equalsIgnoreCase("timeout")) {
                                         try {
                                             return Long.parseLong(value) * 1000;
                                         } catch (NumberFormatException ignore) {
                                         }
                                     }
                                 }
                                 return 60000;
                             }
                         });
             }
         });
         return builder;
     }
 
     private RestClientBuilder setHttpRequest(RestClientBuilder builder) {
         builder.setRequestConfigCallback(
                 new RestClientBuilder.RequestConfigCallback() {
                     @Override
                     public RequestConfig.Builder customizeRequestConfig(
                             RequestConfig.Builder requestConfigBuilder) {
                         return requestConfigBuilder
                                 .setConnectTimeout(30000)
                                 .setSocketTimeout(30000)
                                 .setConnectionRequestTimeout(30000);
                     }
                 });
         return builder;
     }
 
 
     @Bean
     @Scope("singleton")
     public RestHighLevelClient getRestHighLevelClient() {
         LOGGER.info("enter getRestHighLevelClient");
         List<HttpHost> httpList = getHttpHost();
         RestClientBuilder build = RestClient.builder(httpList.toArray(new HttpHost[httpList.size()]));
 
         return new RestHighLevelClient(setHttpRequest(setHttpClient(build)));
     }
 
     @Override
     public void destroy() throws Exception {
         LOGGER.info("enter rest high level client destory");
         if (restHighLevelClient != null) {
             restHighLevelClient.close();
         }
     }
 }
