/*package com.derbysoft.es;


//import com.derbysoft.dlqs.server.DlqsServerApplication;
import org.elasticsearch.action.search.SearchRequest;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.concurrent.*;
*//**
 * @ClassName: ElasticsearchClientTests
 * @Author: nancy
 * @Date: 2019/5/9 14:25
 * @Description:
 *//*
@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = DlqsServerApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application.properties")
public class ElasticsearchClientTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticsearchClient.class);

   @Resource
    private RestHighLevelClient highLevelClient;
   
   @Test
    public void test_client_query() {
        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(QueryBuilders.termQuery("app", "bookingcom1"));
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        sourceBuilder.from(0);
         sourceBuilder.size(5);
         searchRequest.indices("derbysoftaudit-app-*");
         searchRequest.source(sourceBuilder);
         try {
             SearchResponse response = (highLevelClient.search(searchRequest, RequestOptions.DEFAULT));
             System.out.println("test_client_query highLevelClient" + highLevelClient);
             for (SearchHit hit : response.getHits().getHits()) {
                 System.out.println("getResponse:");
                 System.out.println(hit.getSourceAsMap());
             }
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
     
     @Test
     public void test_client() {
     }
     
     @Test
     public void test_client_agg() {
         SearchRequest searchRequest1 = new SearchRequest();
         SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
         sourceBuilder.aggregation(AggregationBuilders.terms("classes").field("action"));
         sourceBuilder.size(0);
         searchRequest1.indices("derbysoft-dstorage-*");
         searchRequest1.source(sourceBuilder);
         try {
             SearchResponse response = (highLevelClient.search(searchRequest1, RequestOptions.DEFAULT));
             System.out.println("test_client_agg highLevelClient" + response.getHits().totalHits);
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
     
 }*/