package com.derbysoft.es;

import com.alibaba.fastjson.JSONObject;



//import com.baozun.omap.dm.auto.frame.annotation.LoginRequired;
import com.carrotsearch.hppc.cursors.ObjectObjectCursor;
import com.derbysoft.util.WriteUtil;

import org.elasticsearch.action.admin.indices.get.GetIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequest;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

/**
 * Created by yi.li on 2018/12/26.
 */
@RestController
@RequestMapping("/api/search")
public class EsModelController {
	
    //@Resource
    private RestHighLevelClient highLevelClient;
	
	@GetMapping("/index")
	public void test_client_query() {
		
		for(int j=0; j<24;j++){
			SearchRequest searchRequest1 = new SearchRequest();
	        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
	        
	        sourceBuilder.aggregation(AggregationBuilders.terms("channelName").field("channel").size(9990));
	        sourceBuilder.size(0);
	        
	        String value ="";
	        
	        if(j<10){
	        	value ="0"+j;
	        }else{
	        	value=""+j;
	        }
	        
	        String startTime ="2019-12-02T"+value+":00:01.001Z";
	        String endTime ="2019-12-02T"+value+":59:59.999Z";
	        sourceBuilder.query(QueryBuilders.rangeQuery("@timestamp").from(startTime).to(endTime));
	        //sourceBuilder.query(QueryBuilders.termQuery("host", "shopstorage-build-us-shop"));
	        searchRequest1.indices("derbysoft-*");
	        searchRequest1.source(sourceBuilder);
	       
	        try {
	            SearchResponse response = (highLevelClient.search(searchRequest1, RequestOptions.DEFAULT));
	            
	            Aggregations aggregations = response.getAggregations();
	            ParsedStringTerms colorTerms = aggregations.get("channelName");
	            List<String> colors = new ArrayList<>();
	            List<? extends Terms.Bucket> buckets = colorTerms.getBuckets();
	                    
	            for (Terms.Bucket bucket : buckets) {
	                 String appName = bucket.getKey().toString();
	                 long count = bucket.getDocCount();
	                 colors.add(appName+":"+count);
	                 WriteUtil.writeTxt("result"+j+".txt",appName+":"+count+"\n");
	            }   
	            
	            System.out.println("test_client_agg highLevelClient" + response.getHits().totalHits);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}
		
    }
	
	@GetMapping("/index2")
	public void test_client_agg() {
		
		for(int j=0; j<24;j++){
			SearchRequest searchRequest1 = new SearchRequest();
			
			BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
			
			SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
	        
			
	        String value ="";
	        
	        if(j<10){
	        	value ="0"+j;
	        }else{
	        	value=""+j;
	        }
	        
	        String startTime ="2019-12-02T"+value+":00:00.001Z";
	        String endTime ="2019-12-02T"+value+":59:59.999Z";
	        
	        MatchPhraseQueryBuilder matchPhraseQueryBuilder = QueryBuilders.matchPhraseQuery("host", "shopstorage-build-us-shop");
			
			RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("@timestamp").from(startTime).to(endTime);
	        
	        boolQueryBuilder.must(rangeQueryBuilder);
	        boolQueryBuilder.must(matchPhraseQueryBuilder);
	        
	        //sourceBuilder.query(QueryBuilders.rangeQuery("@timestamp").from("2019-12-02T00:00:01.001Z").to("2019-12-02T00:59:59.999Z"));
	        //sourceBuilder.query(QueryBuilders.termQuery("host", "shopstorage-build-us-shop"));
	        sourceBuilder.query(boolQueryBuilder);
	        
	        sourceBuilder.aggregation(AggregationBuilders.terms("hotelcodes").field("hotel_supplier").size(9990));
	        
	        //sourceBuilder.size(0);
	        
	        searchRequest1.indices("derbysoft-dstorage-*");
	        searchRequest1.source(sourceBuilder);
	        
	        try {
	            SearchResponse response = (highLevelClient.search(searchRequest1, RequestOptions.DEFAULT));
	            
	            Aggregations aggregations = response.getAggregations();
	            ParsedStringTerms colorTerms = aggregations.get("hotelcodes");
	            List<String> colors = new ArrayList<>();
	            List<? extends Terms.Bucket> buckets = colorTerms.getBuckets();
	                    
	            for (Terms.Bucket bucket : buckets) {
	                 String appName = bucket.getKey().toString();
	                 long count = bucket.getDocCount();
	                 colors.add(appName+":"+count);
	                 WriteUtil.writeTxt("result"+j+".txt",appName+":"+count+"\n");
	            }   
	            
	            System.out.println("test_client_agg highLevelClient" + response.getHits().totalHits);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        
	        try {
				Thread.currentThread().sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		/*
        SearchRequest searchRequest1 = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        
        sourceBuilder.aggregation(AggregationBuilders.terms("hotelcodes").field("hotel_supplier").size(9999));
        sourceBuilder.size(0);
        sourceBuilder.query(QueryBuilders.rangeQuery("@timestamp").from("2019-12-02T00:00:01.001Z").to("2019-12-02T00:59:59.999Z"));
        sourceBuilder.query(QueryBuilders.termQuery("host", "shopstorage-build-us-shop"));
        searchRequest1.indices("derbysoft-dstorage-*");
        searchRequest1.source(sourceBuilder);
       
        try {
            SearchResponse response = (highLevelClient.search(searchRequest1, RequestOptions.DEFAULT));
            
            System.out.println("test_client_agg highLevelClient" + response.getHits().totalHits);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
	
	//https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-date-format.html
    //@Autowired
    //ElasticsearchTemplate elasticsearchTemplate;
    
	/*
    @Autowired
    ProjectServiceImpl projectServiceImpl;
    
    private static final String ANALYZER="simple";
    
    private Logger logger = LoggerFactory.getLogger(EsModelController.class);
    
    SimpleDateFormat simple1 = new SimpleDateFormat("yyyy-MM-dd");
    
    SimpleDateFormat simple2 = new SimpleDateFormat("HH:mm:ss");
    
    SimpleDateFormat simple3 = new SimpleDateFormat("yyyy.MM.dd");
     
    @GetMapping("/index")
    public   Set  getProjects(){
    	Map<String,String> indicesMap=new HashMap<>();
        try{
        	List list = projectServiceImpl.getProjectList();
        	for(int j=0; j<list.size(); j++){
        		Project project =(Project)list.get(j);
        		indicesMap.put(project.getProjectGroup(),project.getProjectGroup_en());
        	}
        	return  indicesMap.keySet();
        }catch (Exception e){
        	return new HashSet<>();
        }
    }
    
    //@LoginRequired
    @GetMapping("/types")
    public List getTypes(String index, String date){
        List<String> typelist=new ArrayList<>();
        try{

            GetMappingsResponse res = null;
            try {
                index+="-"+date.replace("-",".");
                Client client = elasticsearchTemplate.getClient();

                res = client.admin().indices().getMappings(new GetMappingsRequest().indices(index)).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            ImmutableOpenMap<String, MappingMetaData> mapping = res.mappings().get(index);
            for (ObjectObjectCursor<String, MappingMetaData> c : mapping) {
                typelist.add(c.key);
            }
            return typelist;
        }catch (Exception e){
            e.printStackTrace();
            return typelist;
        }
    }
    
    @GetMapping("/getInfo")
    public List getInfo(){
        List<String> typelist=new ArrayList<>();
        try{

            GetMappingsResponse res = null;
            String index ="";
            
            try {
                //index+="-"+date.replace("-",".");
                Client client = elasticsearchTemplate.getClient();
                
                SearchRequestBuilder srb = client.prepareSearch("derbysoft-*");
                srb.setTypes("doc");
                
				HashMap add_map =new HashMap();
				BoolQueryBuilder bqb = QueryBuilders.boolQuery();
				bqb = bqb.must(QueryBuilders.matchQuery("host","shopstorage-build-us-shop"));
				bqb = bqb.must(QueryBuilders.rangeQuery("Time").from("2019-12-01 00:00:01.308").to("2019-12-02 23:59:59.308"));
                
				SortBuilder sortBuilder = SortBuilders.fieldSort("Time").order(SortOrder.ASC);
				
				SearchResponse sr = srb.setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(bqb).setExplain(true).addSort(sortBuilder)
						//.from("2018-12-25T03:04:26.212Z")
						//.to("2018-12-27T03:04:26.212Z")
						.execute().actionGet(); // 查询所有
				
				//res = client.admin().indices().getMappings(new GetMappingsRequest().indices(index)).get();
            } catch (Exception e) {
                e.printStackTrace();
            }
            
            
            ImmutableOpenMap<String, MappingMetaData> mapping = res.mappings().get(index);
            for (ObjectObjectCursor<String, MappingMetaData> c : mapping) {
                typelist.add(c.key);
            }
            
            return typelist;
        }catch (Exception e){
            e.printStackTrace();
            return typelist;
        }
    }
    
    @GetMapping("/annouce")
    public  Map<String,Object> getAnnouceLog(@RequestParam String index,String level,String beginTime,
    		String endTime,String keyword,String type,@RequestParam int page,@RequestParam int pageSize,
    		@RequestParam int maxRowNum,@RequestParam String addJson,@RequestParam String notJson){
        Map<String,Object> returnMap=new HashMap<>();
        try{
        	//这一步是最关键的
            Client client = elasticsearchTemplate.getClient();
            HashMap map = EsUtil.getQueryTimeByTime(beginTime,endTime);
            List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
            
            long allcount = 0;
            
            SearchRequestBuilder srb = client.prepareSearch("derbysoft-dstorage-*");
				srb.setTypes("doc");
				HashMap add_map =new HashMap();
				BoolQueryBuilder bqb = QueryBuilders.boolQuery();
				bqb = bqb.must(QueryBuilders.matchQuery("level.keyword",level));
				
				try {
						bqb = bqb.must(QueryBuilders.rangeQuery("@timestamp")
								.from(String.valueOf(map.get("begin")))
								.to(String.valueOf(map.get("end"))));
						
						WildcardQueryBuilder wildcardqueryBuilder = QueryBuilders.wildcardQuery("message",("*"+keyword+"*").toLowerCase());//.analyzer(ANALYZER)	  

						if(keyword!=null && !"".equals(keyword)){
							
							bqb = bqb.must(
									new QueryStringQueryBuilder(keyword).field("message")
									//QueryBuilders.wildcardQuery(
									 //"message.keyword",("*"+keyword+"*").toLowerCase()
									//)//.analyzer(ANALYZER)	  
							);
						}
						
						if(type!=null && !"".equals(type)){
							bqb = bqb.must(QueryBuilders
									.matchQuery("fields.type", type));
						}
						
						SortBuilder sortBuilder = SortBuilders.fieldSort("@timestamp").order(SortOrder.ASC);
						JSONObject json = new JSONObject();
						HashMap addjsonmap = JsonUtil.getMapByJson(addJson);
						
						if(addjsonmap!=null){
							Iterator iterator = addjsonmap.keySet().iterator();
							while(iterator.hasNext()){
								String key = String.valueOf(iterator.next());
								String value = String.valueOf(addjsonmap.get(key));
								if("hostname".equals(key)){
									bqb = bqb.must(QueryBuilders.matchQuery("host.name.keyword",value));
								}else if("projectname".equals(key)){
									bqb = bqb.must(QueryBuilders.matchQuery("fields.project_name.keyword",value));
								}else if("senvpath".equals(key)){
									bqb = bqb.must(QueryBuilders.matchQuery("s_envpath.keyword",value));
								}else if("level".equals(key)){
									bqb = bqb.must(QueryBuilders.matchQuery("level.keyword",value));
								}else if("type".equals(key)){
									bqb = bqb.must(QueryBuilders.matchQuery("fields.type.keyword",value));
								}
							}
						}
						
						HashMap notjsonmap = JsonUtil.getMapByJson(notJson);
						if(notjsonmap!=null){
							Iterator iterator = notjsonmap.keySet().iterator();
							while(iterator.hasNext()){
								String key = String.valueOf(iterator.next());
								String value = String.valueOf(notjsonmap.get(key));
								if("hostname".equals(key)){
									bqb = bqb.mustNot(QueryBuilders.matchQuery("host.name.keyword",value));
								}else if("projectname".equals(key)){
									bqb = bqb.mustNot(QueryBuilders.matchQuery("fields.project_name.keyword",value));
								}else if("senvpath".equals(key)){
									bqb = bqb.mustNot(QueryBuilders.matchQuery("s_envpath.keyword",value));
								}else if("level".equals(key)){
									bqb = bqb.mustNot(QueryBuilders.matchQuery("level.keyword",value));
								}else if("type".equals(key)){
									bqb = bqb.mustNot(QueryBuilders.matchQuery("fields.type.keyword",value));
								}
							}
						}
						
						SearchResponse sr = srb.setFrom(page).setSize(pageSize)
								.setSearchType(SearchType.DFS_QUERY_THEN_FETCH).setQuery(bqb).setExplain(true).addSort(sortBuilder)
								//.from("2018-12-25T03:04:26.212Z")
								//.to("2018-12-27T03:04:26.212Z")))
								.execute().actionGet(); // 查询所有
						
						SearchHits hits = sr.getHits();
						long count = hits.getTotalHits();
						if("".equals(maxRowNum)||"null".equals(maxRowNum)){
							maxRowNum =0;
						}
						
						for (SearchHit hit : hits) {
							maxRowNum++;
							Map<String, Object> source = hit.getSourceAsMap();
							source.put("id", maxRowNum);
							
							
							if(source!=null){
								if(source.get("msg")!=null && (source.get("message")==null||"".equals(source.get("message")))){
									source.put("message", String.valueOf(source.get("msg")));
									source.remove("msg");
								}else if(source.get("nmsg")!=null && (source.get("message")==null||"".equals(source.get("message")))){
									source.put("message", String.valueOf(source.get("nmsg")));
									source.remove("nmsg");
								}
							}
							
							list.add(source);
							System.out.println(hit.getSourceAsString());
						}
						allcount = count;
						System.out.println("============count==========="+count);
				 } catch (Exception e) {
						e.printStackTrace();
				 }
			
            returnMap.put("count",allcount);
            returnMap.put("data",list);
            return returnMap;
        }catch (Exception e){
            logger.info("查询失败",e);
            List list = new  ArrayList<Map<String, Object>>();
            returnMap.put("count",0);
            returnMap.put("data",list);
            return returnMap;
        }
    }*/
 
}
