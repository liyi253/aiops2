package com.derbysoft.orderforcast.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Reservation implements Serializable{
	
	private static final long serialVersionUID = 3216026240495632457L;
	private Integer id;
	private String ersp;
	private String reservation_state;
	private String channel_code;
	private String supplier_code;
	private String hotel_code;
	private String hotel_name;
	private Date booking_time;
	private String check_in;
	private String check_out;
	private Integer room_nights;
	private Integer bookings;
	private String hotel_reservation_code;
	private Date cancel_time;
	private BigDecimal amount_before_tax;
	private BigDecimal amount_after_tax;
	private String currency;
	private String guest;
	private Date create_time;
	private String channel_reservation_code;
	private Date last_modify_time;
	private String payment_type;
	private String hotel_reservation_code_ext;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getErsp() {
		return ersp;
	}
	
	public void setErsp(String ersp) {
		this.ersp = ersp;
	}
	
	public String getReservation_state() {
		return reservation_state;
	}
	
	public void setReservation_state(String reservation_state) {
		this.reservation_state = reservation_state;
	}
	
	public String getChannel_code() {
		return channel_code;
	}
	
	public void setChannel_code(String channel_code) {
		this.channel_code = channel_code;
	}
	
	public String getSupplier_code() {
		return supplier_code;
	}
	
	public void setSupplier_code(String supplier_code) {
		this.supplier_code = supplier_code;
	}
	
	public String getHotel_code() {
		return hotel_code;
	}
	
	public void setHotel_code(String hotel_code) {
		this.hotel_code = hotel_code;
	}
	
	public String getHotel_name() {
		return hotel_name;
	}
	
	public void setHotel_name(String hotel_name) {
		this.hotel_name = hotel_name;
	}
	
	public Date getBooking_time() {
		return booking_time;
	}
	
	public void setBooking_time(Date booking_time) {
		this.booking_time = booking_time;
	}
	
	public String getCheck_in() {
		return check_in;
	}
	
	public void setCheck_in(String check_in) {
		this.check_in = check_in;
	}
	
	public String getCheck_out() {
		return check_out;
	}
	
	public void setCheck_out(String check_out) {
		this.check_out = check_out;
	}
	
	public Integer getRoom_nights() {
		return room_nights;
	}
	
	public void setRoom_nights(Integer room_nights) {
		this.room_nights = room_nights;
	}
	
	public Integer getBookings() {
		return bookings;
	}
	
	public void setBookings(Integer bookings) {
		this.bookings = bookings;
	}
	
	public String getHotel_reservation_code() {
		return hotel_reservation_code;
	}
	
	public void setHotel_reservation_code(String hotel_reservation_code) {
		this.hotel_reservation_code = hotel_reservation_code;
	}
	
	public Date getCancel_time() {
		return cancel_time;
	}
	
	public void setCancel_time(Date cancel_time) {
		this.cancel_time = cancel_time;
	}
	
	public BigDecimal getAmount_before_tax() {
		return amount_before_tax;
	}
	
	public void setAmount_before_tax(BigDecimal amount_before_tax) {
		this.amount_before_tax = amount_before_tax;
	}
	
	public BigDecimal getAmount_after_tax() {
		return amount_after_tax;
	}
	
	public void setAmount_after_tax(BigDecimal amount_after_tax) {
		this.amount_after_tax = amount_after_tax;
	}
	
	public String getCurrency() {
		return currency;
	}
	
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getGuest() {
		return guest;
	}
	public void setGuest(String guest) {
		this.guest = guest;
	}
	public Date getCreate_time() {
		return create_time;
	}
	public void setCreate_time(Date create_time) {
		this.create_time = create_time;
	}
	public String getChannel_reservation_code() {
		return channel_reservation_code;
	}
	public void setChannel_reservation_code(String channel_reservation_code) {
		this.channel_reservation_code = channel_reservation_code;
	}
	public Date getLast_modify_time() {
		return last_modify_time;
	}
	public void setLast_modify_time(Date last_modify_time) {
		this.last_modify_time = last_modify_time;
	}
	public String getPayment_type() {
		return payment_type;
	}
	public void setPayment_type(String payment_type) {
		this.payment_type = payment_type;
	}
	public String getHotel_reservation_code_ext() {
		return hotel_reservation_code_ext;
	}
	public void setHotel_reservation_code_ext(String hotel_reservation_code_ext) {
		this.hotel_reservation_code_ext = hotel_reservation_code_ext;
	}

}

