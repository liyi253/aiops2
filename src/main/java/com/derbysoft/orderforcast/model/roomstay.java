package com.derbysoft.orderforcast.model;

import java.io.Serializable;

public class roomstay implements Serializable{
	
	private static final long serialVersionUID = -3269767695984807L;
	
	private Integer id;
	private String room_type_code;
	private String room_type_name;
	private String rate_plan_code;
	private String rate_plan_name;
	private String start;
	private String end;
	private String rooms;
	private String reservation_id;
	private String adult;
	private String child;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRoom_type_code() {
		return room_type_code;
	}
	public void setRoom_type_code(String room_type_code) {
		this.room_type_code = room_type_code;
	}
	public String getRoom_type_name() {
		return room_type_name;
	}
	public void setRoom_type_name(String room_type_name) {
		this.room_type_name = room_type_name;
	}
	public String getRate_plan_code() {
		return rate_plan_code;
	}
	public void setRate_plan_code(String rate_plan_code) {
		this.rate_plan_code = rate_plan_code;
	}
	public String getRate_plan_name() {
		return rate_plan_name;
	}
	public void setRate_plan_name(String rate_plan_name) {
		this.rate_plan_name = rate_plan_name;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getRooms() {
		return rooms;
	}
	public void setRooms(String rooms) {
		this.rooms = rooms;
	}
	public String getReservation_id() {
		return reservation_id;
	}
	public void setReservation_id(String reservation_id) {
		this.reservation_id = reservation_id;
	}
	public String getAdult() {
		return adult;
	}
	public void setAdult(String adult) {
		this.adult = adult;
	}
	public String getChild() {
		return child;
	}
	public void setChild(String child) {
		this.child = child;
	}

}
