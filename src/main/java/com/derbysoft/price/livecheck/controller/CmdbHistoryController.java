package com.derbysoft.price.livecheck.controller;

import org.apache.log4j.Logger;

import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.prometheus.client.Counter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;

import com.alibaba.fastjson.JSONArray;
import com.derbysoft.entity.Response;
import com.derbysoft.entity.ResponseResult;
import com.derbysoft.price.livecheck.model.CmdbLog;
import com.derbysoft.price.livecheck.model.InstanceModel;
import com.derbysoft.price.livecheck.model.ServiceNodeMapping;
import com.derbysoft.price.livecheck.model.ServiceQueryCondition;
import com.derbysoft.price.livecheck.repository.CmdbLogRepository;
import com.derbysoft.price.livecheck.repository.ServiceNodeMappingRepository;
import com.derbysoft.price.livecheck.repository.ServiceQueryConditionRepository;
import com.derbysoft.util.JsonUtil;


@RestController
@RequestMapping("/api/price/livecheck")
@Configuration
@EnableScheduling
public class CmdbHistoryController {
	
	private Logger logger = Logger.getLogger(CmdbHistoryController.class);
	
	@Value("${cmdb.getCmdbInfo.authorization}")
    private String authorization;
	
	@Autowired
	private ServiceQueryConditionRepository  serviceQueryConditionRepository;
	
	@Autowired
	private CmdbLogRepository  cmdbLogRepository;
	
	@Autowired
	private ServiceNodeMappingRepository serviceNodeMappingRepository;
	
	//@Scheduled(cron = "0/59 0 0/1 * * ?")
	@GetMapping("/synchronizedCmdb")
	public  boolean synchronizedCmdb(){
		
		SimpleDateFormat simple2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		try{
			List serviceList = serviceQueryConditionRepository.getServiceQueryCondition();
			
			if(serviceList!=null){
				for(int j=0; j<serviceList.size(); j++){
					ServiceQueryCondition serviceQueryCondition = (ServiceQueryCondition)serviceList.get(j);
					
					String serviceid = serviceQueryCondition.getId();
					String application = serviceQueryCondition.getApplication();
					String organization = serviceQueryCondition.getOrganization();
					String environment = serviceQueryCondition.getEnvironment();
					String region = serviceQueryCondition.getRegion();
					String status = serviceQueryCondition.getStatus();
					String pageNumber = "1";
					String pageSize ="0";
					String excludes ="aws_info";
					
					String cmdbInfoUrl = "https://apilayer-proxy.derbysoftsec.com/query/instances?organization="+organization+"&application="+application
							+"&environment="+environment+"&region="+region+"&status="+status
							+"&pageNumber="+pageNumber+"&pageSize="+pageSize+"&excludes="+excludes;
					
					System.out.println("============开始第"+j+"次访问cmdb链接==========");
					
					String info =null;
					
					try{
						info =JsonUtil.getCmdbInfoByUrl(cmdbInfoUrl,authorization);
					}catch(Exception e){
						e.printStackTrace();
						logger.error(e.toString());
						
						try {
							info =JsonUtil.getCmdbInfoByUrl(cmdbInfoUrl,authorization);
						} catch (Exception e1) {
							e1.printStackTrace();
							logger.error(e1.toString());
							try {
								info =JsonUtil.getCmdbInfoByUrl(cmdbInfoUrl,authorization);
							} catch (Exception e2) {
								e2.printStackTrace();
								logger.error(e2.toString());
							}
						}
					}
						
					List<InstanceModel> list = JSONArray.parseArray(info, InstanceModel.class);
					
					Set set = new HashSet();
					
					if(list!=null){
						for(int k=0; k<list.size(); k++){
							InstanceModel instanceModel = (InstanceModel)list.get(k);
							set.add(instanceModel.getInstanceId());
						}
					}
					
					List<ServiceNodeMapping> serviceNodeMappinglist = serviceNodeMappingRepository.getInfoByCondition(serviceid);
					String now = simple2.format(new Date());
					
					if(serviceNodeMappinglist!=null){
						for(int k=0; k<serviceNodeMappinglist.size(); k++){
							ServiceNodeMapping serviceNodeMapping_old = (ServiceNodeMapping)serviceNodeMappinglist.get(k);
							String oldinstanceid = serviceNodeMapping_old.getInstanceId();
							
							if(!set.contains(oldinstanceid)){
								serviceNodeMapping_old.setValidTo(now);
								serviceNodeMapping_old.setActiveType("0");
								
								serviceNodeMappingRepository.save(serviceNodeMapping_old);//更新有效期结束时间
							}
						}
					}
					
					if(list!=null){
						for(int i=0; i<list.size(); i++){
							InstanceModel instancesModel = list.get(i);
							
							if(serviceQueryCondition.getClustercode()!=null && serviceQueryCondition.getClustercode().equals(instancesModel.getClusterCode())){
								List<Object[]> serviceNodeMappingList = serviceNodeMappingRepository.getInfoByCondition(instancesModel.getApplication(),instancesModel.getClusterCode(),instancesModel.getInstanceId());
								
								if(serviceNodeMappingList==null || serviceNodeMappingList.size()==0){ 
									ServiceNodeMapping serviceNodeMapping_new = new ServiceNodeMapping();
									serviceNodeMapping_new.setServiceId(serviceid);
									serviceNodeMapping_new.setInstanceId(instancesModel.getInstanceId());
									serviceNodeMapping_new.setValidFrom(now);
									serviceNodeMapping_new.setValidTo("2299-12-31 23:59:59");
									serviceNodeMapping_new.setActiveType("1");
										
									serviceNodeMappingRepository.save(serviceNodeMapping_new);
								}
								
							}
						}
					}
					
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}	
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return false;
    }
	
	
	private static Random random = new Random();

    private static final Counter requestTotal = Counter.build()
        .name("my_sample_counter")
        .labelNames("status")
        .help("A simple Counter to illustrate custom Counters in Spring Boot and Prometheus").register();
    
    @RequestMapping("/endpoint")
    public void endpoint() {
        if (random.nextInt(2) > 0) {
            requestTotal.labels("success").inc();
        } else {
            requestTotal.labels("error").inc();
        }
    }
	
	/*
	@GetMapping("/getCmdbInfoByCondition") //根据一个时间段获取所有的信息
	public  ResponseResult<List<ServiceNodeMapping>> getCmdbInfoByCondition(@RequestParam(value = "begintime", required = true) String begintime,
			@RequestParam(value = "endtime", required = true) String endtime){
			//SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List newlist = new ArrayList();
			
			begintime = begintime+" 00:00:00";
			endtime = endtime+" 23:59:59";
			
			try {
				List<ServiceQueryCondition> list = serviceQueryConditionRepository.getServiceQueryCondition();
				
				List list2 = new ArrayList();
				
				if(list!=null){
					for(int j=0; j<list.size(); j++){
						ServiceQueryCondition serviceQueryCondition =(ServiceQueryCondition)list.get(j);
						String service_id = serviceQueryCondition.getId();
						List<Object[]>  objectList = serviceNodeMappingRepository.getServiceNodeMappingCondition(begintime, endtime,service_id);
					
						newlist.add(objectList);
					}
				}
				
				return Response.makeOKRsp(newlist);
			} catch (Exception e) {
				e.printStackTrace();
				return Response.makeErrRsp("获取统计信息异常");
			}
	}
	
	@GetMapping("/getCmdbHistoryByCondition") //根据一个时间段获取所有的信息
	public  ResponseResult<List<CmdbLog>> getCmdbHistoryByCondition(@RequestParam(value = "begintime", required = true) String begintime,
			@RequestParam(value = "endtime", required = true) String endtime){
			SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			List newlist = new ArrayList();
			
			begintime = begintime+" 00:00:00";
			endtime = endtime+" 23:59:59";
			
			try {
				List<CmdbLog> list = cmdbLogRepository.getCmdbHistoryByCondition(begintime,endtime);
				if(list!=null){
					for(int j=0; j<list.size(); j++){
						CmdbLog  cmdbLog = (CmdbLog)list.get(j);
						
						if(cmdbLog.getValidto()==null||"".equals(cmdbLog.getValidto())){//validto为空
							cmdbLog.setValidto(endtime);
						}else{
							String validto = cmdbLog.getValidto();
							String validfrom = cmdbLog.getValidfrom();
							
							Date  validfromdate = simple.parse(validfrom);
							Date  validtodate = simple.parse(validto);
							
							Date  beginDate = simple.parse(begintime);
							Date  endDate = simple.parse(endtime);
							
							if(validfromdate.before(beginDate)){
								cmdbLog.setValidfrom(begintime);
							}else{
								cmdbLog.setValidfrom(validfrom);
							}
							
							if(validtodate.before(endDate)){
								cmdbLog.setValidto(validto);
							}else{
								cmdbLog.setValidto(endtime);
							}
						}
						newlist.add(cmdbLog);
					}
				}
				
				return Response.makeOKRsp(newlist);
			} catch (Exception e) {
				e.printStackTrace();
				return Response.makeErrRsp("获取统计信息异常");
			}
	}
	
	
	//@Scheduled(cron = "0/59 0 0/1 * * ?")
	@GetMapping("/synchronizationCmdb")
	public  boolean synchronizationCmdb(){
		List keylist = new ArrayList();
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat simple2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		StringBuilder sb = new StringBuilder();
		List dataList = new ArrayList();
		
		List serviceList = serviceQueryConditionRepository.getServiceQueryCondition();
		
		if(serviceList!=null){
			for(int j=0; j<serviceList.size(); j++){
				
				ServiceQueryCondition serviceQueryCondition = (ServiceQueryCondition)serviceList.get(j);
				
				String application = serviceQueryCondition.getApplication();
				String organization = serviceQueryCondition.getOrganization();
				String environment = serviceQueryCondition.getEnvironment();
				String region = serviceQueryCondition.getRegion();
				String status = serviceQueryCondition.getStatus();
				String pageNumber = "1";
				String pageSize ="0";
				String excludes ="aws_info";
				
				
				String cmdbInfoUrl = "https://apilayer-proxy.derbysoftsec.com/query/instances?organization="+organization+"&application="+application
				+"&environment="+environment+"&region="+region+"&status="+status
				+"&pageNumber="+pageNumber+"&pageSize="+pageSize+"&excludes="+excludes;
						
				System.out.println("============开始第"+j+"次访问cmdb链接==========");
				
				
				String info =null;
				
				try{
					info =JsonUtil.getCmdbInfoByUrl(cmdbInfoUrl,authorization);
				}catch(Exception e){
					e.printStackTrace();
					try {
						info =JsonUtil.getCmdbInfoByUrl(cmdbInfoUrl,authorization);
					} catch (Exception e1) {
						e1.printStackTrace();
						try {
							info =JsonUtil.getCmdbInfoByUrl(cmdbInfoUrl,authorization);
						} catch (Exception e2) {
							e2.printStackTrace();
						}
					}
				}
					
				List<InstanceModel> list = JSONArray.parseArray(info, InstanceModel.class);
				Long mainInstance_id = 0L;
				List instanceList = new ArrayList();
						
				Map map = new HashMap();
				
				if(list!=null){
					for(int i=0; i<list.size(); i++){
						InstanceModel instances = list.get(i);
						List<Object[]> listcount =null;
								
						String instanceid = instances.getInstanceId();
						String clustercode = instances.getClusterCode();//clustercode
								
						//先检查主表是否存在
						if(map.get(clustercode)==null){
							map.put(clustercode, instanceid);
						}else{
							String oldinstanceids = String.valueOf(map.get(clustercode));
							map.put(clustercode, oldinstanceids+","+instanceid);
						}
					}
				}
				
				if(map!=null){
					Set set = map.keySet();
					Iterator iterator = set.iterator();
					
					while(iterator.hasNext()){
						String clustercode = String.valueOf(iterator.next());
						String newinstanceids = String.valueOf(map.get(clustercode));
						
						//把数据库中那条数据找出来
						List<CmdbLog> cmdbLogList = cmdbLogRepository.getInfoByCondition(application,clustercode);
						boolean isExist =false;
						CmdbLog cmdbLog = null;
						
						if(cmdbLogList!=null && cmdbLogList.size()>0){ //根据application和clustercode查询validto不为空的
							//用下面的判断是原因就是jpa在查询的时候有个bug,条件查询的时候不区分大小写，比如clustercode为Dswitch Router,和DSwitch Router jpa是不区分的所以还要在这里判断下
							for(int i =0; i<cmdbLogList.size(); i++){
								CmdbLog cmdbLog2 = cmdbLogList.get(i);
								if(cmdbLog2.getClustercode().equals(clustercode)&& cmdbLog2.getApplication().equals(application)){
									cmdbLog = cmdbLog2;
									isExist = true;
								}
							}
							if(isExist==true && cmdbLog!=null){
								
								String oldinstanceids = cmdbLog.getInstanceids();
								//比较oldinstanceids和newinstanceids里面的值是否相同
								String [] oldInstanceidArray  = oldinstanceids.split(",");
								String [] newInstanceidArray  = newinstanceids.split(",");
								
								Arrays.sort(oldInstanceidArray);
								Arrays.sort(newInstanceidArray);
								
								if (!Arrays.equals(oldInstanceidArray, newInstanceidArray)) { 
									//两个数组中的元素值不相同做修改
									cmdbLog.setValidto(simple2.format(new Date()));
									cmdbLogRepository.save(cmdbLog);//更新有效期结束时间
									
									CmdbLog cmdbLog_new = new CmdbLog();
									cmdbLog_new.setApplication(application);
									cmdbLog_new.setClustercode(clustercode);
									cmdbLog_new.setOrganization(organization);
									cmdbLog_new.setRegion(region);
									cmdbLog_new.setInstanceids(newinstanceids);
									cmdbLog_new.setNum(newInstanceidArray.length+"");
									cmdbLog_new.setValidfrom(simple2.format(new Date()));
									cmdbLog_new.setValidto(null);
									
									cmdbLogRepository.save(cmdbLog_new);
								} 
							}else{
								String [] newInstanceidArray  = newinstanceids.split(",");
								int size =0;
								
								if(newInstanceidArray!=null){
									size = newInstanceidArray.length;
								}
								
								CmdbLog cmdbLog_new = new CmdbLog();
								cmdbLog_new.setApplication(application);
								cmdbLog_new.setOrganization(organization);
								cmdbLog_new.setRegion(region);
								cmdbLog_new.setNum(size+"");
								cmdbLog_new.setClustercode(clustercode);
								cmdbLog_new.setInstanceids(newinstanceids);
								cmdbLog_new.setValidfrom(simple2.format(new Date()));
								
								cmdbLogRepository.save(cmdbLog_new);
							}
						
						}else{
							String [] newInstanceidArray  = newinstanceids.split(",");
							int size =0;
							
							if(newInstanceidArray!=null){
								size = newInstanceidArray.length;
							}
							
							CmdbLog cmdbLog3 = new CmdbLog();
							cmdbLog3.setApplication(application);
							cmdbLog3.setOrganization(organization);
							cmdbLog3.setRegion(region);
							cmdbLog3.setNum(size+"");
							cmdbLog3.setClustercode(clustercode);
							cmdbLog3.setInstanceids(newinstanceids);
							cmdbLog3.setValidfrom(simple2.format(new Date()));
							//cmdbLog.setValidto(null);
							
							cmdbLogRepository.save(cmdbLog3);
						}
					}
				}
				
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}	
			}
		}
		
		return false;
    }
	*/
	
}
