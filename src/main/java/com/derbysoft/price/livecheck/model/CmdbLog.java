package com.derbysoft.price.livecheck.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class CmdbLog implements Serializable{

private static final long serialVersionUID = 1L;
	
	@Id 
	@GenericGenerator(name="generatsr",strategy="increment") //设置增长方式
	@GeneratedValue(generator="generatsr")
	private Long id;
	
	private String organization;
	private String application;
	private String clustercode;
	private String region;
	
	@Column(name = "instanceids", nullable = false, columnDefinition = "varchar(6000)")
	private String instanceids;
	private String num;
	private String validfrom;
	private String validto;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getOrganization() {
		return organization;
	}
	
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getApplication() {
		return application;
	}
	
	public void setApplication(String application) {
		this.application = application;
	}
	
	public String getClustercode() {
		return clustercode;
	}
	
	public void setClustercode(String clustercode) {
		this.clustercode = clustercode;
	}
	
	public String getRegion() {
		return region;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getInstanceids() {
		return instanceids;
	}
	
	public void setInstanceids(String instanceids) {
		this.instanceids = instanceids;
	}
	
	public String getValidfrom() {
		return validfrom;
	}
	
	public void setValidfrom(String validfrom) {
		this.validfrom = validfrom;
	}
	
	public String getValidto() {
		return validto;
	}
	
	public void setValidto(String validto) {
		this.validto = validto;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

}
