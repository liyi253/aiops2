package com.derbysoft.price.livecheck.model;

import java.io.Serializable;



public class InstanceModel implements Serializable{

	private static final long serialVersionUID = 1L;

	private String id;
	private String organization;//STONE
	private String application;//ShopStorage
	private String environment;//product
	private String region;//us-west-2
	private String status;//running
	private String time;
	private String instanceId;//5d2308a00053383ea95239e8
	private String name;//ShopStorage-pika
	private String clusterCode;//build_us_shop
	private String engineers;//leo.gu
	
	

	public String getClusterCode() {
		return clusterCode;
	}

	public void setClusterCode(String clusterCode) {
		this.clusterCode = clusterCode;
	}

	public String getEngineers() {
		return engineers;
	}

	public void setEngineers(String engineers) {
		this.engineers = engineers;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOrganization() {
		return organization;
	}
	
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getApplication() {
		return application;
	}
	
	public void setApplication(String application) {
		this.application = application;
	}
	
	public String getEnvironment() {
		return environment;
	}
	
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
	public String getRegion() {
		return region;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}

}
