package com.derbysoft.price.livecheck.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class ServiceNodeMapping implements Serializable{
	
	private static final long serialVersionUID = 8611753431155726339L;
	
	@Id 
	@GenericGenerator(name="generatsr",strategy="increment") //设置增长方式
    @GeneratedValue(generator="generatsr")
	private long id;
	private String serviceId;
	private String instanceId;
	private String validFrom;
	private String validTo;
	private String activeType;
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getServiceId() {
		return serviceId;
	}
	
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}
	
	public String getInstanceId() {
		return instanceId;
	}
	
	public void setInstanceId(String instanceId) {
		this.instanceId = instanceId;
	}
	
	public String getValidFrom() {
		return validFrom;
	}
	
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	
	public String getValidTo() {
		return validTo;
	}
	
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	
	public String getActiveType() {
		return activeType;
	}
	public void setActiveType(String activeType) {
		this.activeType = activeType;
	}
}
