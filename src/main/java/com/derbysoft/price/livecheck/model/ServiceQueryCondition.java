package com.derbysoft.price.livecheck.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class ServiceQueryCondition implements Serializable{
	
	private static final long serialVersionUID = 8611753431155726339L;
	
	@Id 
	@GenericGenerator(name="generatsr",strategy="increment") //设置增长方式
    @GeneratedValue(generator="generatsr")
	private String id;
	private String organization;
	private String application;
	private String clustercode;
	private String costunit;
	private String environment;
	private String region;
	private String instancetype;
	private String status;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOrganization() {
		return organization;
	}
	
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
	public String getApplication() {
		return application;
	}
	
	public void setApplication(String application) {
		this.application = application;
	}
	
	
	public String getEnvironment() {
		return environment;
	}
	
	public void setEnvironment(String environment) {
		this.environment = environment;
	}
	
	public String getRegion() {
		return region;
	}
	
	public void setRegion(String region) {
		this.region = region;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public String getClustercode() {
		return clustercode;
	}

	public void setClustercode(String clustercode) {
		this.clustercode = clustercode;
	}

	public String getCostunit() {
		return costunit;
	}

	public void setCostunit(String costunit) {
		this.costunit = costunit;
	}

	public String getInstancetype() {
		return instancetype;
	}

	public void setInstancetype(String instancetype) {
		this.instancetype = instancetype;
	}
	
}
