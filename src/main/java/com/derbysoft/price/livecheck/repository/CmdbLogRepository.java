package com.derbysoft.price.livecheck.repository;
//package om.derbysoft.orderforcast.repository;
//
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.derbysoft.price.livecheck.model.CmdbLog;



public interface CmdbLogRepository extends JpaRepository<CmdbLog,Long>{
	
    @Query(value = " select * from cmdb_log  where application = (:application) and clustercode = (:clustercode) and validto is null ", nativeQuery = true)	
	List<CmdbLog> getInfoByCondition(@Param(value = "application") String application,@Param(value = "clustercode") String clustercode);

    //这里有两种情况：第一种情况是:validto为空，表示当前都还在运行，还有一种情况是validto不为空
    //当validto为空的时候,就需要判断validfrom是否小于endtime，这样validfrom-validto与begintime-endtime有交集
    //当validto不为空的时候,就要判断validfrom-validto与begintime-endtime必须有交集
    @Query(value = " select * from cmdb_log  where ((validfrom >=(:begintime) and validfrom <=(:endtime)) or (validto >=(:begintime) and validfrom <=(:endtime) and validto is not null ) or (validfrom <=(:endtime) and validto is  null ))  order by application,clustercode,validfrom ", nativeQuery = true)	
   	List<CmdbLog> getCmdbHistoryByCondition(@Param(value = "begintime") String begintime,@Param(value = "endtime") String endtime);

}
