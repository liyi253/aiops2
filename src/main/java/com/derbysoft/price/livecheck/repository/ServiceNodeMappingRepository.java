package com.derbysoft.price.livecheck.repository;

import java.util.List;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.derbysoft.price.livecheck.model.ServiceNodeMapping;

public interface ServiceNodeMappingRepository extends JpaRepository<ServiceNodeMapping,Long>{
	
	@Query(value = " select a.service_id, a.instance_id, b.change_day from service_node_mapping as a "+
	" cross join  (select * from (select valid_from as change_day from service_node_mapping "+
	" where (:begintime) <= valid_from  AND (:endtime) >= valid_from AND service_id = (:service_id)"+
	" union select valid_to as change_day from service_node_mapping  where (:begintime) <= valid_to "+
	" AND (:endtime) >= valid_to AND service_id =(:service_id)) tn UNION  select (:begintime) "+
	" UNION select (:endtime)) as b where b.change_day >= a.valid_from AND (b.change_day < a.valid_to ) "+
	" AND service_id = (:service_id) order by b.change_day, a.service_id ", nativeQuery = true)	
	List<Object[]> getServiceNodeMappingCondition(@Param(value = "begintime") String begintime,
			@Param(value = "endtime") String endtime,@Param(value = "service_id") String service_id);

	@Query(value = " select t2.application,t2.clustercode,t1.instance_id,t1.id,t1.service_id,t1.valid_from,"
			+ " t1.valid_to,t1.active_type from service_node_mapping t1, service_query_condition t2 "
			+ " where t2.application = (:application)  and t2.clustercode = (:clustercode) "
			+ " and t1.instance_id =(:instance_id) and t2.id = t1.service_id and t1.active_type ='1' "
			+ " order by valid_from desc", nativeQuery = true)	
	List<Object[]> getInfoByCondition(@Param(value = "application") String application,
			@Param(value = "clustercode") String clustercode,@Param(value = "instance_id") String instance_id);
	
	@Query(value = " select * from service_node_mapping where service_id = (:serviceid) and active_type = 1 ", nativeQuery = true)	
	List<ServiceNodeMapping> getInfoByCondition(@Param(value = "serviceid") String serviceid);
	
	

	
}
