package com.derbysoft.price.livecheck.repository;
//package om.derbysoft.orderforcast.repository;
//
import java.util.List;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.derbysoft.price.livecheck.model.ServiceQueryCondition;


public interface ServiceQueryConditionRepository extends JpaRepository<ServiceQueryCondition,Long>{
	
	
	@Query(value = "select  t.* from service_query_condition t", nativeQuery = true)	
	List<ServiceQueryCondition> getServiceQueryCondition();
	
	@Query(value = "select  t.* from service_query_condition t where t2.application = (:application)  and t2.clustercode = (:clustercode) ", nativeQuery = true)	
	List<ServiceQueryCondition> getServiceQueryCondition(@Param(value = "application") String application,@Param(value = "clustercode") String clustercode);
	
}
