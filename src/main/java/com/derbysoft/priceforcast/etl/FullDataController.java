package com.derbysoft.priceforcast.etl;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.sf.json.JSONObject;

import com.alibaba.fastjson.JSONArray;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeDTO;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRS;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.OccupancyRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.RatesDTO;
import com.derbysoft.dswitch.dto.hotel.cds.Type;
import com.derbysoft.dswitch.dto.hotel.common.DateRangeDTO;
import com.derbysoft.dswitch.dto.hotel.common.FeesDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.dswitch.remote.hotel.exception.TimeoutException;
import com.derbysoft.priceforcast.model.HashMapCollection;
import com.derbysoft.priceforcast.model.Hotelnfo;
import com.derbysoft.util.CSVUtil;
import com.derbysoft.util.Json2Map;
import com.derbysoft.util.JsonUtil;
import com.derbysoft.util.RedisClient;
import com.derbysoft.util.S3Util;
import com.derbysoft.util.WriteUtil;

@RestController
@RequestMapping("/api2/priceforcast/etl")
public class FullDataController {
	private static final Logger LOGGER = LoggerFactory.getLogger(FullDataController.class);
	
	@Value("${dmx.getHotelbyProvider.url}")
    private String dmxGetHotelbyProviderurl;
	
    @Value("${hotelInfo.ipport}")
    private String hotelInfoIpPort;
	
	public static DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService_dmx1 = new DefaultHotelBuyerRemoteService("52.41.157.240:9002");
	
	@GetMapping("/allimport")
	public  List getAllLosPriceInfo(){
		HotelLOSRateChangeRequest hotLOSRateChangeRequest = new HotelLOSRateChangeRequest();
		LOSRateChangeRQ losRateChangeRQ = new LOSRateChangeRQ();
		List keylist = new ArrayList();
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
		
		System.out.println("========进入getAllLosPriceInfo=====");
		
		List list = getAllHotelCodeList();
		
		int fileIndex =1;
		//String fileName = "data0.txt";
		String supplier = "MARRIOTT";//
		
		StringBuilder sb = new StringBuilder();
		List dataList = new ArrayList();
		
		if(list!=null){
			for(int i=0; i<list.size(); i++){
				long starttime = System.currentTimeMillis();
				dataList.add("supply@hotelcode@roomType@ratePlan@los@adult@child@checkintime@time@price");
				String hotelcode = String.valueOf(list.get(i));
				
				for(int r=0; r<60; r++){
					Calendar cal = Calendar.getInstance(); 
					cal.add(cal.DATE, r);
					Date date = cal.getTime();
					String checkintime = simple.format(date);
					
					long startTime =-1;
					String losRate = null;
					long endTime =-1;
					
					try {
						startTime = System.currentTimeMillis();
						losRate = JsonUtil.getLosRateByCondition(supplier, hotelcode, checkintime);
						endTime = System.currentTimeMillis();
					} catch (Exception e1) {
						r--;
						System.out.println("timeout出现异常 supplier:"+supplier+"---hotelcode:"+hotelcode+"---checkintime"+checkintime);
						continue;
						//e1.printStackTrace();
					}
					System.out.println("i="+i+",r="+r+",查询花费的时间为:"+(endTime-startTime)+"毫秒");
					
					Map map = Json2Map.json2Map(losRate);
					
					try{
						
						JSONObject jsonObject0= (JSONObject)map.get("value");
						net.sf.json.JSONArray jsonArray=  (net.sf.json.JSONArray)jsonObject0.get("LosRoomRates");
						
						for(int j=0; j<jsonArray.size(); j++){
							JSONObject jsonobject1 = (JSONObject)jsonArray.get(j);
							JSONObject roomRateCodeObject = (JSONObject)jsonobject1.get("roomRateCode");
							String roomType ="";//参数2
							String ratePlan ="";
							
							if(roomRateCodeObject!=null){
								roomType = String.valueOf(roomRateCodeObject.get("roomType"));
								ratePlan = String.valueOf(roomRateCodeObject.get("ratePlan"));
							}
							
							JSONObject losRatesObject =  (JSONObject)jsonobject1.get("losRates");
							JSONObject ratesObject = (JSONObject)losRatesObject.get("rates");
							
							HashMap index_price_map =new HashMap();
							JSONObject rateObj0 = (JSONObject)ratesObject.get("rates");
							
							Set set = rateObj0.keySet();
							Iterator iterator = set.iterator();
									
							while(iterator.hasNext()){
								String index = String.valueOf(iterator.next());//index
								JSONObject rateObj1 = (JSONObject)rateObj0.get(index);
								String beforeTax = String.valueOf(rateObj1.get("beforeTax"));//税前收入
								index_price_map.put(index, beforeTax);
							}
							
							net.sf.json.JSONArray losRatesArray = (net.sf.json.JSONArray)losRatesObject.get("losRates");
							
							if(losRatesArray!=null){
								for(int k=0; k<losRatesArray.size(); k++){
									JSONObject jsonobject2 = (JSONObject)losRatesArray.get(k);
									
									String los = String.valueOf(jsonobject2.get("los")); //参数3
									net.sf.json.JSONArray occupancyRatesArray =  (net.sf.json.JSONArray)jsonobject2.get("occupancyRates");
									
									if(occupancyRatesArray!=null){
										for(int m=0; m<occupancyRatesArray.size(); m++){
											JSONObject jsonobject3  = (JSONObject)occupancyRatesArray.get(m);
											String occupancy = String.valueOf(jsonobject3.get("occupancy"));
											String adult ="";
											String child ="";
											if("100".equals(occupancy)){//1-0:
												adult ="1"; child ="0";
											}else if("200".equals(occupancy)){//2-0:
												adult ="2"; child ="0";
											}else if("300".equals(occupancy)){//3-0:
												adult ="3"; child ="0";
											}else if("400".equals(occupancy)){//4-0:
												adult ="4"; child ="0";
											}else if("500".equals(occupancy)){//5-0:
												adult ="5"; child ="0";
											}else if("600".equals(occupancy)){//6-0:
												adult ="6"; child ="0";
											}else if("700".equals(occupancy)){//7-0:
												adult ="7"; child ="0";
											}else if("800".equals(occupancy)){//8-0:
												adult ="8"; child ="0";
											}else if("900".equals(occupancy)){//9-0:
												adult ="9"; child ="0";
											}
											
											String dailyRateIndexes = String.valueOf(jsonobject3.get("dailyRateIndexes"));
											
											if(dailyRateIndexes!=null && dailyRateIndexes.startsWith("[") && dailyRateIndexes.endsWith("]")){
												dailyRateIndexes = dailyRateIndexes.substring(1, dailyRateIndexes.length()-1);
												String [] indexArray = dailyRateIndexes.split(",");
												
												String price ="";
												if(indexArray!=null){
													for(int q=0; q<indexArray.length; q++){
														if(index_price_map.get(String.valueOf(indexArray[q]))!=null){
															String price_beforeTax = String.valueOf(index_price_map.get(String.valueOf(indexArray[q])));
															//price+= (price_beforeTax+"/ ");
															price+= (price_beforeTax+"*");
														}
													}
												}
												
												if(price!=null){
													if(price.endsWith("*")){
														price = price.substring(0, price.length()-1);
													}
												}
												
												String key ="";
												HashMapCollection.priceHashMap.put(supplier+"@"+hotelcode+"@"+roomType+"@"+ratePlan+"@"+los+"@"+adult+"@"+child+"@"+checkintime+"@"+simple.format(new Date()), price);
												String str =supplier+"@"+hotelcode+"@"+roomType+"@"+ratePlan+"@"+los+"@"+adult+"@"+child+"@"+checkintime+"@"+simple.format(new Date()+"@"+price);
												
												dataList.add(str);
											}
										}
									}
								}
								losRatesArray.size();
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				
				//String toFilePath ="/home/xQWoLyOLxMbtajSNM4Z/liyi/priceforcast/"+supplier+"/"+hotelcode+"/"+simple.format(new Date())+"/data.csv";
				String toFilePath ="C:/workspace/priceforcast/"+supplier+"/"+hotelcode+"/"+simple.format(new Date())+"/data.csv";
				
				File file = new File(toFilePath);
				
				File dir = file.getParentFile();
				if (!dir.exists()) {
					dir.mkdirs();
				}
				try {
					file.createNewFile();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				CSVUtil.importCsv(dataList,toFilePath);
				dataList = new ArrayList();
				long endtime = System.currentTimeMillis();
				System.out.println("第"+i+"次获取信息,持续时间:"+(endtime-starttime)/1000+"秒");
			}
		}
		
		return null;
	}
	
	
	@GetMapping("/downloadS3")
	public void downloadS3(@RequestParam String time) {
		System.out.println("========进入downloadS3=====");
	
    	String supplier = "MARRIOTT";
    	SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
    	List hotelCodeList = getAllHotelCodeList();
    	S3Util  s3Util = new S3Util();
    	
    	if(time==null||"".equals(time)){
			time = simple.format(new Date());
		}
    	
    	try {
    		if(hotelCodeList!=null){
    			for(int j=0 ; j<hotelCodeList.size(); j++){
    				String hotelcode = String.valueOf(hotelCodeList.get(j));
    				s3Util.downloadS3(hotelcode,time);
    			}
    		}
    	} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	@GetMapping("/uploadS3")
	public void uploadS3(@RequestParam String time) {
		System.out.println("========进入uploadS3=====");
		
		String supplier = "MARRIOTT";
    	SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
    	List hotelCodeList = getAllHotelCodeList();
    	
    	if(time==null||"".equals(time)){
			time = simple.format(new Date());
		}
    	
    	for(int j=0; j<hotelCodeList.size(); j++){
    		String hotelcode = String.valueOf(hotelCodeList.get(j));
    		
    		String tempFile ="/home/xQWoLyOLxMbtajSNM4Z/liyi/priceforcast/"+supplier+"/"+hotelcode+"/"+time+"/data.csv";
    		String remoteFileName ="/priceforcast3/"+supplier+"/"+hotelcode+"/"+time+"/data.csv";
    		
    		S3Util s3Util = new S3Util();
    		
    		try {
    			System.out.println("=====tempFile======"+tempFile);
    			System.out.println("=====remoteFileName======"+remoteFileName);
    			
				s3Util.uploadToS3(new File(tempFile), remoteFileName);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					s3Util.uploadToS3(new File(tempFile), remoteFileName);//上传失败,继续上传
				} catch (Exception e1) {
					e1.printStackTrace();
					try {
						s3Util.uploadToS3(new File(tempFile), remoteFileName);//上传失败,继续上传
					} catch (Exception e2) {
						e2.printStackTrace();
					}
				}
			}
    	}
    	
    	System.err.println("执行静态定时任务时间: " + LocalDateTime.now());
    }
	
	@Configuration      //1.主要用于标记配置类，兼备Component的效果。
	@EnableScheduling   // 2.开启定时任务
	public class uploadS3ScheduleTask {
	    //3.添加定时任务
	    //@Scheduled(cron = "1 0 0 1/1 * ?")
	    private void upload() {
	    	SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	SimpleDateFormat simple2 = new SimpleDateFormat("yyyy-MM-dd");
	    	System.out.println("进入uploadS3的时间:"+simple.format(new Date()));
	    	uploadS3(simple2.format(new Date()));
	    }
	}
	
	@Configuration      //1.主要用于标记配置类，兼备Component的效果。
	@EnableScheduling   // 2.开启定时任务
	public class getAllLosPriceInfoS3ScheduleTask {
	    //3.添加定时任务
	    //@Scheduled(cron = "1 0 1 1/1 * ?")
	    private void getAllLosPrice() {
	    	SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	System.out.println("进入getAllLosPriceInfo的时间:"+simple.format(new Date()));
	    	getAllLosPriceInfo();
	    }
	}
	
	/**获取所有的酒店code**/
	public List getAllHotelCodeList(){
		
		String hotelCodeArray[] =  
			   {"NYCTR","NYCTS","NYCTM","NYCTX","NYCTW","NYCTE","NYCTF","NYCSK","NYCSM","NYCSW","NYCST",
				"NYCSG","NYCSH","NYCRL","NYCRT","NYCRI","NYCRD","NYCRE","NYCXR","NYCXM","NYCNU","NYCOF",
				"NYCWS","NYCWD","NYCWH","NYCVC","NYCZW","NYCEA","NYCEB","NYCEL","NYCDS","NYCDF","NYCCP",
				"NYCCL","NYCCE","NYCBR","NYCAK","NYCAL","NYCHW","NYCHA","NYCFT","NYCFO","NYCFM","NYCFI",
				"NYCES","NYCET","NYCEX","NYCME","NYCMF","NYCMH","NYCMQ","NYCMS","NYCMT","NYCMM"/*,"NYCMO",
				"NYCMP","NYCLX","NYCLT","NYCMA","NYCMD","NYCLH","NYCLR","NYCLM","NYCJJ","NYCPK","NYCPR",
				"NYCPS","NYCOX","NYCPC","NYCOS","NYCOT","NYCMW"*/};
		List list = new ArrayList();
		
		for(int j=0; j<hotelCodeArray.length; j++){
			list.add(hotelCodeArray[j]);
		}
		
		return list;
		
	}
}
