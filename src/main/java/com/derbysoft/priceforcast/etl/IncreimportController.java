package com.derbysoft.priceforcast.etl;

import static org.elasticsearch.common.unit.TimeValue.timeValueMillis;

import com.derbysoft.dswitch.dto.hotel.cds.DailyRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeDTO;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRS;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.OccupancyRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.RatesDTO;
import com.derbysoft.dswitch.dto.hotel.common.DateRangeDTO;
import com.derbysoft.dswitch.dto.hotel.common.FeesDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.dswitch.remote.hotel.exception.TimeoutException;
import com.derbysoft.priceforcast.GetLosPriceInfo;
import com.derbysoft.util.ArrayUtil;
import com.derbysoft.util.DateUtil;
import com.derbysoft.util.WriteUtil;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.action.admin.indices.get.GetIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequest;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.derbysoft.dswitch.dto.hotel.cds.Type;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.Resource;

/**
 * Created by yi.li on 2018/12/26.
*/ 
@RestController
@RequestMapping("/api/priceforcast/etl_bak")
public class IncreimportController {
	
    //@Resource
    private RestHighLevelClient highLevelClient;
    
    private static long num = 0;
    
    public static DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService_dmx = new DefaultHotelBuyerRemoteService("52.41.157.240:9002");
	
	@GetMapping("/increment")
	public void test_client_query() {
		
		long starttime_long = System.currentTimeMillis();
		
		HashMap map = getLastOneHoursChangePriceFromEs();
		HashMap querymap = null;
		HashMap changetimemap = null;
		
		if(map!=null && map.get("0")!=null){
			querymap = (HashMap)map.get("0");
		}
		
		if(map!=null && map.get("1")!=null){
			changetimemap = (HashMap)map.get("1");
		}
		
		List list = getLosPriceInfo(defaultHotelBuyerRemoteService_dmx,querymap,changetimemap);//根据参数获取酒店信息
		
		long endtime_long = System.currentTimeMillis();
		
		System.out.print("=====总的查询时间:===="+(endtime_long-starttime_long)/1000+"秒\n");
	}
	
	
	
	//获取近一个小时的数据,
	public HashMap getLastOneHoursChangePriceFromEs(){
		HashMap querymap = new HashMap();
		HashMap changetimemap = new HashMap();
		SearchHit [] searchHitArray_all = new SearchHit [100];
		
		SearchRequest searchRequest1 = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        
        SimpleDateFormat simple_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simple_time = new SimpleDateFormat("HH:mm:ss");
        
        Calendar cal = Calendar.getInstance();
        cal.add(cal.HOUR, -8);//如果是本地执行当前时区往前推8个小时，如果是在生产环境中运行这段代码可以去掉
        
        StringBuilder sb = new  StringBuilder();
        
        for(int i=1 ;i<=20; i++){//分20次把近1个小时的数据取出来
        	if(i==1){
        		//cal.add(cal.MINUTE,-180);//780分钟之前
        		cal.add(cal.HOUR,-24);//780分钟之前
        	}
        	
            String startDate = simple_date.format(cal.getTime());//开始日期
            String startTime = simple_time.format(cal.getTime());//开始时间
            
            //cal.add(cal.MINUTE,1);//719分钟之前
            cal.add(cal.HOUR,1);//719分钟之前
            
            String endDate = simple_date.format(cal.getTime());//结束日期
            String endTime = simple_time.format(cal.getTime());//结束时间
            
            String startStr = startDate+"T"+startTime+".001Z";
            String endStr = endDate+"T"+endTime+".000Z";
            
            String allTime = startStr+"@"+endStr+",";
            
            sb.append(allTime);
        }
        
        String time = sb.toString();
        
        if(time!=null && time.endsWith(",")){
        	time = time.substring(0, time.length()-1);
        }
        
        String[] timeArray = time.split(",");
        
        for(int j=0 ; j<timeArray.length; j++){
        	String timestr = timeArray[j];
        	String [] strArray = timestr.split("@");
        	String startStr =  strArray[0];
        	String endStr  = strArray[1];
        				
        	QueryBuilder queryBuilder0 = QueryBuilders.matchPhraseQuery("message", "rate.diff.result=Y"); 
            QueryBuilder queryBuilder1 = QueryBuilders.rangeQuery("@timestamp").from(startStr).to(endStr);
            QueryBuilder queryBuilder2 = QueryBuilders.termQuery("process", "ARIDiff");
            
            /*
            QueryBuilder queryBuilder11 = QueryBuilders.termQuery("hotel_suppier", "NYCTR");
            QueryBuilder queryBuilder12 = QueryBuilders.termQuery("hotel_suppier", "NYCTS");
            QueryBuilder queryBuilder13 = QueryBuilders.termQuery("hotel_suppier", "NYCTM");
            QueryBuilder queryBuilder14 = QueryBuilders.termQuery("hotel_suppier", "NYCTX");
            
            QueryBuilder queryBuilder3 = QueryBuilders.boolQuery().should(queryBuilder11).should(queryBuilder12).should(queryBuilder13).should(queryBuilder14);
            */
            BoolQueryBuilder bqb1 = QueryBuilders.boolQuery().must(queryBuilder0).must(queryBuilder1).must(queryBuilder2);//.must(queryBuilder3);
            sourceBuilder.query(bqb1);
            
            sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
            sourceBuilder.from(0);
            sourceBuilder.size(50000);
            
            searchRequest1.indices("derbysoft-marriott-*");
            searchRequest1.source(sourceBuilder);
            
            try {
            	long starttime_long = System.currentTimeMillis();
				
            	Scroll scroll = new Scroll(timeValueMillis(360000L));
            	searchRequest1.scroll(scroll);
                SearchResponse response = highLevelClient.search(searchRequest1);
            	
                long totalHits = response.getHits().totalHits;
                SearchHit [] searchHitArray = response.getHits().getHits();
                long endtime_long = System.currentTimeMillis();
				System.out.print("=====查询时间:"+j+"===="+(endtime_long-starttime_long)+"毫秒\n");
				
				
				if(searchHitArray!=null){
	            	for(int i=0; i<searchHitArray.length;i++){
	            		SearchHit searchHit = (SearchHit)searchHitArray[i];
	            		if(searchHit!=null){
	            			Map map = searchHit.getSourceAsMap();
	                		String supplier  = String.valueOf(map.get("supplier")); //1:供应商
	                		String hotel_supplier  = String.valueOf(map.get("hotel_supplier"));//2:酒店code
	                		String los  = String.valueOf(map.get("los")); //3:los
	                		String checkin  = String.valueOf(map.get("check_in"));//4:入住时间
	                		String message  = String.valueOf(map.get("message"));
	                		
	                		int indexstart =message.indexOf("<rate.plan.candidate=");
	                		int indexend =message.indexOf("><last.update.time");
	                		String rateplan = message.substring(indexstart+21, indexend);//5:房型
	                		
	                		int changeIndexstart =message.indexOf("<last.change.time=");
	                		int changeIndexend =message.indexOf("><rate.diff.result");
	                		String changeTime = message.substring(changeIndexstart+18, changeIndexend);//最后更新时间
	                		
	                		//把更新时间和
	                		if(changetimemap.get(supplier+"@"+hotel_supplier+"@"+los+"@"+checkin+"@"+rateplan)==null){//不是重复的数据
	                			changetimemap.put((supplier+"@"+hotel_supplier+"@"+los+"@"+checkin+"@"+rateplan),changeTime);
	                			
	                			if(querymap.get(supplier+"@"+hotel_supplier+"_los@checkin")!=null){
	                    			String checkin_str = String.valueOf(querymap.get(supplier+"@"+hotel_supplier+"_los@checkin"));
	                    			querymap.put(supplier+"@"+hotel_supplier+"_los@checkin", checkin_str+","+checkin);
	                    		}else{
	                    			querymap.put(supplier+"@"+hotel_supplier+"_los@checkin", checkin);
	                    		}
	                			
	                			if(querymap.get(supplier+"@"+hotel_supplier+"_los")!=null){
	                    			String los_str = String.valueOf(querymap.get(supplier+"@"+hotel_supplier+"_los"));
	                    			querymap.put(supplier+"@"+hotel_supplier+"_los", los_str+","+los);
	                    		}else{
	                    			querymap.put(supplier+"@"+hotel_supplier+"_los", los);
	                    		}
	                			
	                			if(querymap.get(supplier+"@"+hotel_supplier+"_rateplan")!=null){
	                    			String rateplan_str = String.valueOf(querymap.get(supplier+"@"+hotel_supplier+"_rateplan"));
	                    			querymap.put(supplier+"@"+hotel_supplier+"_rateplan", rateplan_str+","+rateplan);
	                    		}else{
	                    			querymap.put(supplier+"@"+hotel_supplier+"_rateplan", rateplan);
	                    		}
	                		}
	            		}
	            	}
	            }
				
            } catch (IOException e) {
                e.printStackTrace();
            }			
        }
        
        HashMap resultMap = new HashMap();
        resultMap.put("0", querymap);
        resultMap.put("1", changetimemap);
        
        return resultMap;
	}
	
	public static List getLosPriceInfo(DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService,HashMap map,HashMap changetimemap){
		
		HotelLOSRateChangeRequest hotLOSRateChangeRequest = new HotelLOSRateChangeRequest();
		
		LOSRateChangeRQ losRateChangeRQ = new LOSRateChangeRQ();
		
		List keylist = new ArrayList();
		
		if(map!=null){
			Set set = map.keySet();
			if(set!=null){
				Iterator iterator = set.iterator();
				while(iterator.hasNext()){
					String key = String.valueOf(iterator.next());
					if(key.endsWith("_los")==true){
						keylist.add(key);
					}
				}
			}
		}
		
		String fileName = "modifyData.txt";
		StringBuilder sb = new StringBuilder();
		
		//把keylist中的数据根据checkin合并下
		
		if(keylist!=null && keylist.size()>0){
			for(int j=0; j<keylist.size(); j++){
				String key = String.valueOf(keylist.get(j));
				String value = String.valueOf(map.get(key));//value是由
				String checkinvalue = String.valueOf(map.get(key+"@checkin"));//value是由
				
				String hotelcode = "";
				String checkin ="";
				String supplier ="";//供应商
				
				if(key!=null){
					String [] keystr = key.split("@");
					supplier = keystr[0];
					hotelcode = keystr[1];
				}
				
				List losList = new ArrayList();
				if(value!=null){
					String[] losstr = value.split(",");
					HashMap losMap =new HashMap();
					
					if(losstr!=null && losstr.length>0){
						for(int k=0 ;k<losstr.length; k++){
							if(losMap.get(losstr[k])==null){
								losMap.put(losstr[k], "0");
								losList.add(Integer.parseInt(losstr[k]));//这样做是防止重复
							}	
						}
					}
				}
				
				List checkinList = new ArrayList();
				if(checkinvalue!=null){
					String[] checkinstr = checkinvalue.split(",");
					
					if(checkinstr.length>=2){
						System.out.println("========checkinstr.length========"+checkinstr.length);
					}
					
					HashMap checkMap =new HashMap();
					
					if(checkinstr!=null && checkinstr.length>0){
						for(int k=0 ;k<checkinstr.length; k++){
							if(checkMap.get(checkinstr[k])==null){
								checkMap.put(checkinstr[k], "0");
								checkinList.add(checkinstr[k]);//这样做是防止重复
							}	
						}
					}
				}
				
				
				List rateplanList = new ArrayList();
				String rateplanstr = String.valueOf(map.get(supplier+"@"+hotelcode+"@"+"_rateplan"));
				
				if(rateplanstr!=null){
					String [] str = rateplanstr.split(",");
					HashMap rateplanMap =new HashMap();
					
					if(str!=null){
						for(int i=0; i<str.length; i++){
							if(rateplanMap.get(str[i])==null){
								rateplanMap.put(str[i], "0");
								rateplanList.add(str[i]);//这样做是防止重复
							}
						}
					}
					
					rateplanMap = null;//释放内存
				}
				
				DateUtil dateUtil = new DateUtil();
				List dateList = null;
				
				try {
					
					System.out.println("合并之前日期数量:"+checkinList.size());
					dateList = dateUtil.getDateList(dateUtil.search(checkinList));
					System.out.println("合并之后日期数量:"+dateList.size());
				
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				for(int i=0; i<dateList.size(); i++){
					Map<String,Object> datemap1 = (HashMap)dateList.get(i);
					
					String checkinbegin = String.valueOf(datemap1.get("begin"));
					String checkinend = String.valueOf(datemap1.get("end"));
					
					DateRangeDTO dateRangeDTO = new DateRangeDTO();
					dateRangeDTO.setStart(checkinbegin);
					dateRangeDTO.setEnd(checkinend);
					
					num++;
					
					losRateChangeRQ.setDateRange(dateRangeDTO);
					losRateChangeRQ.setHotelCode(hotelcode);
					losRateChangeRQ.setLosList(losList);
					losRateChangeRQ.setType(Type.All);
					losRateChangeRQ.setRatePlansList(rateplanList);
					
					hotLOSRateChangeRequest.setHeader(new RequestHeader("hhfd", supplier, "DDFF"));
					hotLOSRateChangeRequest.setLosRateChangeRQ(losRateChangeRQ);
					
					defaultHotelBuyerRemoteService_dmx.setSyncChangeTimeout(1);
					defaultHotelBuyerRemoteService_dmx.setNoCacheAvailTimeout(1);
					defaultHotelBuyerRemoteService_dmx.setReservationTimeout(1);
					defaultHotelBuyerRemoteService_dmx.setAvailTimeout(1);
					
					HotelLOSRateChangeResponse hotelLOSRateChangeResponse =null;
					
					try {
						long starttime_long = System.currentTimeMillis();
						hotelLOSRateChangeResponse = defaultHotelBuyerRemoteService_dmx.getLOSRateChange(hotLOSRateChangeRequest);
						long endtime_long = System.currentTimeMillis();
						
						System.out.print("=====查询时间:"+j+"===="+(endtime_long-starttime_long)+"毫秒\n");
					} catch (TimeoutException e) {
						e.printStackTrace();
						continue;
					}
					
					LOSRateChangeRS  lOSRateChangeRS=hotelLOSRateChangeResponse.getLosRateChangeRS();
					
					if(lOSRateChangeRS!=null){
						
					List losRateChangeslist = lOSRateChangeRS.getLosRateChangesList();
					
					if(losRateChangeslist!=null){
						for(int m=0; m<losRateChangeslist.size(); m++){
							LOSRateChangeDTO lOSRateChangeDTO = (LOSRateChangeDTO)losRateChangeslist.get(m);
							List  losRatesList = lOSRateChangeDTO.getLosRatesList();
							String ratePlanCode = lOSRateChangeDTO.getRatePlanCode();//关键字2:ratePlanCode
							String roomTypeCode = lOSRateChangeDTO.getRoomTypeCode();//roomTypeCode
							String checkInDate = lOSRateChangeDTO.getCheckInDate();//checkInDate
							
							if(losRatesList!=null){
								for(int n=0; n<losRatesList.size(); n++){
									LOSRateDTO  losRateDTO = (LOSRateDTO)losRatesList.get(n);
									FeesDTO FeeDTO = losRateDTO.getFees();
									int los = losRateDTO.getLos();//关键字3:天数
									
									List<RatesDTO> rateList =   losRateDTO.getRatesList();
									
									if(rateList!=null){
										for(int h=0; h<rateList.size(); h++){
											RatesDTO rateDTO  = rateList.get(h);
											List<OccupancyRateDTO> occupancyRateDTOList =rateDTO.getOccupancyRatesList();
											
											Double extraAdultAmt = 0d;
											Double extraChildAmt = 0d;
											
											if(rateDTO.getBaseByGuestAmts()!=null){
												extraAdultAmt = rateDTO.getBaseByGuestAmts().getExtraAdultAmt();//关键字4:额外成年人数量，额外小孩数量
												extraChildAmt = rateDTO.getBaseByGuestAmts().getExtraChildAmt();
											}
											
											Integer adult = 0;
											Integer childNum = 0;
											
											String changetime = String.valueOf(changetimemap.get(supplier+"@"+hotelcode+"@"+checkInDate+"@"+ratePlanCode));
											
											String price = hotelcode+","+supplier+","+ratePlanCode+","+roomTypeCode+","+changetime+","+los+",";
											
											if(occupancyRateDTOList!=null){
												for(int g =0; g<occupancyRateDTOList.size(); g++){
													OccupancyRateDTO  occupancyRateDTO = occupancyRateDTOList.get(g);
													Double amountAfterTax = occupancyRateDTO.getAmountAfterTax();//关键字5:税后，税前，成年人，大人
													Double amountBeforeTax = occupancyRateDTO.getAmountBeforeTax();
													
													if(g==0){
														adult = occupancyRateDTO.getAdult();
			    										childNum =  occupancyRateDTO.getChild();
													}
													
													String amountAfterTax_str="";
													String amountBeforeTax_str ="";
													
													if(amountAfterTax==null){
														amountAfterTax_str =""; 
													}else{
														amountAfterTax_str =""+amountAfterTax; 
													}
													
													if(amountBeforeTax==null){
														amountBeforeTax_str ="";
													}else{
														amountBeforeTax_str =""+amountBeforeTax;
													}
													
													price += amountBeforeTax_str+"/"+amountAfterTax_str+",";
												}
											}
											
											if(price!=null && price.endsWith(",")){
												price = price.substring(0, price.length()-1)+","+adult+":"+childNum+","+extraAdultAmt+","+extraChildAmt+"\n";
											}
											sb.append(price);
										}
									}
									WriteUtil.writeTxt(sb.toString(),fileName);
								}
							}
						}
					 }
				   }
				}
			}
		}
		
		sb = new StringBuilder();
		
		System.out.println("=====num======"+num);
		
		return null;
	}
	
	
	/**获取所有的酒店code**/
	public List getAllHotelCodeList(){
		String hotelCodeArray[] =  
			   {"NYCTR","NYCTS","NYCTM","NYCTX","NYCTW","NYCTE","NYCTF","NYCSK","NYCSM","NYCSW","NYCST",
				"NYCSG","NYCSH","NYCRL","NYCRT","NYCRI","NYCRD","NYCRE","NYCXR","NYCXM","NYCNU",
				"NYCWS","NYCWD","NYCWH","NYCVC","NYCZW","NYCEA","NYCEB","NYCEL","NYCDS","NYCDF","NYCCP",
				"NYCCL","NYCCE","NYCBR","NYCAK","NYCAL","NYCHW","NYCHA","NYCFT","NYCFO","NYCFM","NYCFI",
				"NYCES","NYCET","NYCEX","NYCME","NYCMF","NYCMH","NYCMQ","NYCMS","NYCMT","NYCMM","NYCMO",
				"NYCMP","NYCLX","NYCLT","NYCMA","NYCMD","NYCLH","NYCLR","NYCLM","NYCJJ","NYCPK","NYCPR",
				"NYCPS","NYCOX","NYCPC","NYCOS","NYCOT","NYCOF","NYCMW"}; 
		
		List list = new ArrayList();
		
		for(int j=0; j<hotelCodeArray.length; j++){
			list.add(hotelCodeArray[j]);
		}
		
		return list;
	}

}
