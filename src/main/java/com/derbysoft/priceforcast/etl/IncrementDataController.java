package com.derbysoft.priceforcast.etl;

import static org.elasticsearch.common.unit.TimeValue.timeValueMillis;

import com.derbysoft.dswitch.dto.hotel.cds.DailyRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeDTO;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRS;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.OccupancyRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.RatesDTO;
import com.derbysoft.dswitch.dto.hotel.common.DateRangeDTO;
import com.derbysoft.dswitch.dto.hotel.common.FeesDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelDailyRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.dswitch.remote.hotel.exception.TimeoutException;
import com.derbysoft.priceforcast.GetLosPriceInfo;
import com.derbysoft.priceforcast.model.HashMapCollection;
import com.derbysoft.util.ArrayUtil;
import com.derbysoft.util.CSVUtil;
import com.derbysoft.util.DateUtil;
import com.derbysoft.util.WriteUtil;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.action.admin.indices.get.GetIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsRequest;
import org.elasticsearch.action.admin.indices.mapping.get.GetMappingsResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.cluster.metadata.MappingMetaData;
import org.elasticsearch.common.collect.ImmutableOpenMap;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchPhraseQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.index.query.WildcardQueryBuilder;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.derbysoft.dswitch.dto.hotel.cds.Type;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.annotation.Resource;

/**
 * Created by yi.li on 2018/12/26.
*/ 
@RestController
@RequestMapping("/api2/priceforcast/etl")
public class IncrementDataController {
	
    //@Resource
    private RestHighLevelClient highLevelClient;
    
	private long expireTime = 86400000;
    
    private static long num = 0;
    
    public static DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService_dmx = new DefaultHotelBuyerRemoteService("52.41.157.240:9002");
	
	@GetMapping("/increment")
	public void test_client_query() {
		
		long starttime_long = System.currentTimeMillis();
		
		HashMap querymap = new HashMap();
		HashMap changetimemap = new HashMap();
		SearchHit [] searchHitArray_all = new SearchHit [100];
		
		SearchRequest searchRequest1 = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        
        SimpleDateFormat simple_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simple_time = new SimpleDateFormat("HH:mm:ss");
        
        Calendar cal = Calendar.getInstance();
        cal.add(cal.HOUR, -8);//如果是本地执行当前时区往前推8个小时，如果是在生产环境中运行这段代码可以去掉
        
        StringBuilder sb = new  StringBuilder();
        
        for(int i=1 ;i<=20; i++){//分20次把近1个小时的数据取出来
        	if(i==1){
        		//cal.add(cal.MINUTE,-180);//780分钟之前
        		cal.add(cal.HOUR,-24);//780分钟之前
        	}
        	
            String startDate = simple_date.format(cal.getTime());//开始日期
            String startTime = simple_time.format(cal.getTime());//开始时间
            
            //cal.add(cal.MINUTE,1);//719分钟之前
            cal.add(cal.HOUR,1);//719分钟之前
            
            String endDate = simple_date.format(cal.getTime());//结束日期
            String endTime = simple_time.format(cal.getTime());//结束时间
            
            String startStr = startDate+"T"+startTime+".001Z";
            String endStr = endDate+"T"+endTime+".000Z";
            
            String allTime = startStr+"@"+endStr+",";
            
            sb.append(allTime);
        }
        
        String time = sb.toString();
        
        if(time!=null && time.endsWith(",")){
        	time = time.substring(0, time.length()-1);
        }
        
        String[] timeArray = time.split(",");
        
        for(int j=0 ; j<timeArray.length; j++){
        	String timestr = timeArray[j];
        	String [] strArray = timestr.split("@");
        	String startStr =  strArray[0];
        	String endStr  = strArray[1];
        				
        	QueryBuilder queryBuilder0 = QueryBuilders.matchPhraseQuery("message", "rate.diff.result=Y"); 
            QueryBuilder queryBuilder1 = QueryBuilders.rangeQuery("@timestamp").from(startStr).to(endStr);
            QueryBuilder queryBuilder2 = QueryBuilders.termQuery("process", "ARIDiff");
            
            //QueryBuilder queryBuilder11 = QueryBuilders.termQuery("hotel_suppier", "NYCTR");
            //QueryBuilder queryBuilder3 = QueryBuilders.boolQuery().should(queryBuilder11);
            
            BoolQueryBuilder bqb1 = QueryBuilders.boolQuery().must(queryBuilder0).must(queryBuilder1).must(queryBuilder2);//.must(queryBuilder3);
            sourceBuilder.query(bqb1);
            
            sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
            sourceBuilder.from(0);
            sourceBuilder.size(50000);
            
            searchRequest1.indices("derbysoft-marriott-*");
            searchRequest1.source(sourceBuilder);
            
            try {
				
            	Scroll scroll = new Scroll(timeValueMillis(360000L));
            	searchRequest1.scroll(scroll);
                SearchResponse response = highLevelClient.search(searchRequest1);
            	
                long totalHits = response.getHits().totalHits;
                SearchHit [] searchHitArray = response.getHits().getHits();
                long endtime_long = System.currentTimeMillis();
				System.out.print("=====查询时间:"+j+"===="+(endtime_long-starttime_long)+"毫秒\n");
				
				if(searchHitArray!=null){
	            	for(int i=0; i<searchHitArray.length;i++){
	            		SearchHit searchHit = (SearchHit)searchHitArray[i];
	            		if(searchHit!=null){
	            			Map map = searchHit.getSourceAsMap();
	            			
	                		String supplier  = String.valueOf(map.get("supplier")); //1:供应商
	                		String hotel_supplier  = String.valueOf(map.get("hotel_supplier"));//2:酒店code
	                		String los  = String.valueOf(map.get("los")); //3:los
	                		String checkin  = String.valueOf(map.get("check_in"));//4:入住时间
	                		String message  = String.valueOf(map.get("message"));
	                		String roomType = String.valueOf(map.get("roomType"));//房间类型
	                		String rateplan = String.valueOf(map.get("rateplan"));
	                		String adult = String.valueOf(map.get("adult"));
	                		String child = String.valueOf(map.get("child"));
	                		String price  = String.valueOf(map.get("price"));//最新的价格
	                		
	                		int changeIndexstart =message.indexOf("<last.change.time=");
	                		int changeIndexend =message.indexOf("><rate.diff.result");
	                		
	                		//String changeTime = message.substring(changeIndexstart+18, changeIndexend);//最后更新时间
	                		String key = supplier+"@"+hotel_supplier+"@"+roomType+"@"+rateplan+"@"+los+"@"+adult+"@"+child+"@"+checkin+"@"+simple_date.format(new Date());
	                		
	                		HashMapCollection.priceHashMap.put(key, price);
	                	}
	            	}
	            }
			} catch (IOException e) {
                e.printStackTrace();
            }			
        }
	}
	
	@GetMapping("/exportfile")
	public void exportfile() {
		long starttime_long = System.currentTimeMillis();
		
		HashMap querymap = new HashMap();
		HashMap changetimemap = new HashMap();
		SearchHit [] searchHitArray_all = new SearchHit [100];
		
		SearchRequest searchRequest1 = new SearchRequest();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        
        SimpleDateFormat simple_date = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat simple_time = new SimpleDateFormat("HH:mm:ss");
        
        Calendar cal = Calendar.getInstance();
        cal.add(cal.HOUR, -8);//如果是本地执行当前时区往前推8个小时，如果是在生产环境中运行这段代码可以去掉
        
        StringBuilder sb = new  StringBuilder();
        List list = new  ArrayList();
        
        String key = "supplier@hotel_supplier@los@checkin@time@price@changeTime";
		
		list.add(key);
		int k=0;
		
        
        for(int i=1 ;i<=2016; i++){//分2016次把近60个小时的数据取出来
        	if(i==1){
        		//cal.add(cal.MINUTE,-180);//780分钟之前
        		cal.add(cal.DATE,-7);//20天以前
        	}
        	
            String startDate = simple_date.format(cal.getTime());//开始日期
            String startTime = simple_time.format(cal.getTime());//开始时间
            
            //cal.add(cal.MINUTE,1);//719分钟之前
            
            cal.add(cal.MINUTE,5);//719分钟之前
            
            
            
            String endDate = simple_date.format(cal.getTime());//结束日期
            String endTime = simple_time.format(cal.getTime());//结束时间
            
            String startStr = startDate+"T"+startTime+".001Z";
            String endStr = endDate+"T"+endTime+".000Z";
            
            String allTime = startStr+"@"+endStr+",";
            
            sb.append(allTime);
        }
        
        String time = sb.toString();
        
        if(time!=null && time.endsWith(",")){
        	time = time.substring(0, time.length()-1);
        }
        
        String[] timeArray = time.split(",");
        
        for(int j=0 ; j<timeArray.length; j++){
        	String timestr = timeArray[j];
        	String [] strArray = timestr.split("@");
        	String startStr =  strArray[0];
        	String endStr  = strArray[1];
        				
        	QueryBuilder queryBuilder0 = QueryBuilders.matchPhraseQuery("message", "rate.diff.result=Y"); 
            QueryBuilder queryBuilder1 = QueryBuilders.rangeQuery("@timestamp").from(startStr).to(endStr);
            QueryBuilder queryBuilder2 = QueryBuilders.termQuery("process", "ARIDiff");
            
            //QueryBuilder queryBuilder11 = QueryBuilders.termQuery("hotel_suppier", "NYCTR");
            //QueryBuilder queryBuilder3 = QueryBuilders.boolQuery().should(queryBuilder11);
            
            BoolQueryBuilder bqb1 = QueryBuilders.boolQuery().must(queryBuilder0).must(queryBuilder1).must(queryBuilder2);//.must(queryBuilder3);
            sourceBuilder.query(bqb1);
            
            sourceBuilder.timeout(new TimeValue(120, TimeUnit.SECONDS));
            sourceBuilder.from(0);
            sourceBuilder.size(50000);
            
            searchRequest1.indices("derbysoft-marriott-*");
            searchRequest1.source(sourceBuilder);
            
            try {
				
            	Scroll scroll = new Scroll(timeValueMillis(360000L));
            	searchRequest1.scroll(scroll);
                SearchResponse response = highLevelClient.search(searchRequest1);
            	
                long totalHits = response.getHits().totalHits;
                SearchHit [] searchHitArray = response.getHits().getHits();
                long endtime_long = System.currentTimeMillis();
				System.out.print("=====查询时间:"+j+"===="+(endtime_long-starttime_long)+"毫秒\n");
				
				if(searchHitArray!=null){
	            	for(int i=0; i<searchHitArray.length;i++){
	            		SearchHit searchHit = (SearchHit)searchHitArray[i];
	            		if(searchHit!=null){
	            			Map map = searchHit.getSourceAsMap();
	            			
	                		String supplier  = String.valueOf(map.get("supplier")); //1:供应商
	                		String hotel_supplier  = String.valueOf(map.get("hotel_supplier"));//2:酒店code
	                		String los  = String.valueOf(map.get("los")); //3:los
	                		String checkin  = String.valueOf(map.get("check_in"));//4:入住时间
	                		String message  = String.valueOf(map.get("message"));
	                		
	                		String roomType = String.valueOf(map.get("roomType"));//房间类型
	                		String rateplan = String.valueOf(map.get("rateplan"));
	                		String adult = String.valueOf(map.get("adult"));
	                		String child = String.valueOf(map.get("child"));
	                		String price  = String.valueOf(map.get("price"));//最新的价格
	                		
	                		int changeIndexstart =message.indexOf("<last.change.time=");
	                		int changeIndexend =message.indexOf("><rate.diff.result");
	                		
	                		String changeTime = message.substring(changeIndexstart+18, changeIndexend);//最后更新时间
	                		String key1 = supplier+"@"+hotel_supplier+"@"+los+"@"+checkin+"@"+simple_date.format(new Date())+"@"+changeTime;
	                		
	                		list.add(key1);
	                		
	                		if(list.size()==10000000){
	                			CSVUtil.importCsv(list,"C:/workspace/priceforcast/change"+k+".csv");
	                			k++;
	                	        list = new ArrayList();
	                	        list.add("supplier@hotel_supplier@los@checkin@time@price@changeTime");
	                		}
	                	}
	            	}
	            }
			} catch (IOException e) {
                e.printStackTrace();
            }			
        }
        CSVUtil.importCsv(list,"C:/workspace/priceforcast/change"+k+".csv");
        list = null;
	}
	
	
	@GetMapping("/downloadfile")
	public void downloadfile() {
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
		HashMap supplyhotelHashMap = new HashMap();
		Set set = HashMapCollection.priceHashMap.keySet();
		Iterator it = set.iterator();
		
		while(it.hasNext()){
			try {
				String key = String.valueOf(it.next());
				
				String price = String.valueOf(HashMapCollection.priceHashMap.get(key));
				
				String [] str = key.split("@");
				String supply = String.valueOf(str[0]);
				String hotelcode = String.valueOf(str[1]);
				
				Object value = HashMapCollection.getFileHashMap.get(supply+"@"+hotelcode);
				List valueList = null;
				
				if(value!=null){
					valueList = (List)(value);
				}else{
					valueList = new ArrayList();
					valueList.add("supply@hotelcode@roomType@ratePlan@los@adult@child@checkintime@time@price");
				}
				
				valueList.add(key+"@"+price);
				
				HashMapCollection.getFileHashMap.put(supply+"@"+hotelcode,valueList);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		HashMap hashmap = HashMapCollection.getFileHashMap;
		
		if(hashmap!=null){
			Set keyset = hashmap.keySet();
			Iterator iterator = keyset.iterator();
			
			while(iterator.hasNext()){
				String key = String.valueOf(iterator.next());
				List dataList = (List)HashMapCollection.getFileHashMap.get(key);
				
				String[] keystr = key.split("@");
				
				String supplier = String.valueOf(keystr[0]);
				String hotelcode = String.valueOf(keystr[1]);
				
				String toFilePath ="C:/workspace/priceforcast/"+supplier+"/"+hotelcode+"/"+simple.format(new Date())+"/data.csv";
				
				File file = new File(toFilePath);
				
				File dir = file.getParentFile();
				if (!dir.exists()) {
					dir.mkdirs();
				}
				try {
					file.createNewFile();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				CSVUtil.importCsv(dataList,toFilePath);
			}
		}
	}
	
	/**获取所有的酒店code**/
	public List getAllHotelCodeList(){
		String hotelCodeArray[] =  
			   {"NYCTR","NYCTS","NYCTM","NYCTX","NYCTW","NYCTE","NYCTF","NYCSK","NYCSM","NYCSW","NYCST",
				"NYCSG","NYCSH","NYCRL","NYCRT","NYCRI","NYCRD","NYCRE","NYCXR","NYCXM","NYCNU",
				"NYCWS","NYCWD","NYCWH","NYCVC","NYCZW","NYCEA","NYCEB","NYCEL","NYCDS","NYCDF","NYCCP",
				"NYCCL","NYCCE","NYCBR","NYCAK","NYCAL","NYCHW","NYCHA","NYCFT","NYCFO","NYCFM","NYCFI",
				"NYCES","NYCET","NYCEX","NYCME","NYCMF","NYCMH","NYCMQ","NYCMS","NYCMT","NYCMM","NYCMO",
				"NYCMP","NYCLX","NYCLT","NYCMA","NYCMD","NYCLH","NYCLR","NYCLM","NYCJJ","NYCPK","NYCPR",
				"NYCPS","NYCOX","NYCPC","NYCOS","NYCOT","NYCOF","NYCMW"}; 
		
		List list = new ArrayList();
		
		for(int j=0; j<hotelCodeArray.length; j++){
			list.add(hotelCodeArray[j]);
		}
		
		return list;
	}
	
	@GetMapping("/addData")
	public void addData() {
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM0@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "380");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM1@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "381");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM2@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "382");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM3@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "383");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM4@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "384");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM5@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "385");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM6@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "386");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM7@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "387");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM8@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "388");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM9@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "389");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM10@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "390");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM11@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "391");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM12@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "392");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM13@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "393");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM14@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "394");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM15@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "395");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM16@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "396");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM17@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "397");
		HashMapCollection.priceHashMap.put("MARRIOTT@NYCMM18@DCTY@BKSH@1@1@0@2020-01-14@2020-01-14", "398");
	}

}
