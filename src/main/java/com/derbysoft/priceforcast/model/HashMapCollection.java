package com.derbysoft.priceforcast.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import com.derbysoft.util.DateUtil;


public class HashMapCollection {
	
	public SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
	
	public static HashMap priceHashMap = new HashMap();
	
	public static HashMap getFileHashMap = new HashMap();
	
	public HashMap getPriceHashMap() {
		return priceHashMap;
	}

	public void setPriceHashMap(HashMap priceHashMap) {
		this.priceHashMap = priceHashMap;
	}

	public HashMapCollection() {
		Timer timer = new Timer();
        TimerTask task = new TimerTask() { // 创建一个新的计时器任务。
            
        	@Override
            public void run() {
                List<String> removeList = new ArrayList<String>();
                Set set = priceHashMap.keySet();
                Iterator it = set.iterator();
                
                while(it.hasNext()){
                	String key = String.valueOf(it.next());
                	int lastindex = key.lastIndexOf("-");
                	int length = key.length();
                	
                	String checkinTime = key.substring(lastindex, length);
                	Date today = new Date();
                	
                	try {
                		Date checkinDate = simple.parse(checkinTime);//checkin的日期
                		int tianshu =  DateUtil.getTianshu(checkinDate, new Date());
                		
                		if(tianshu>0){
                			removeList.add(key);
                		}
                	} catch (ParseException e) {
						e.printStackTrace();
					};
                }
                removeExpireData(removeList);
            }
        };
        
        timer.schedule(task, 2000, 86400000); // 一分钟刷新一次
    }
	
    /**
     * 移除过期数据
     * 
     * @param keys
     */
    public void removeExpireData(List<String> keys) {
        for (String key : keys) {
        	priceHashMap.remove(key);
        }
    }
}
