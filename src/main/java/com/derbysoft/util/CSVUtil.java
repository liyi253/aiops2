package com.derbysoft.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 ** CSV操作(读取和写入) 
 ** @author liyi  
 ** @version 2019-12-31   
 */
public class CSVUtil {

	public static boolean importCsv(File file, List<String> dataList) {
		boolean isSucess = false;

		FileOutputStream out = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
		try {
			out = new FileOutputStream(file);
			osw = new OutputStreamWriter(out,"UTF-8");
			
			//DataInputStream in = new DataInputStream(new FileInputStream(new File("d:\\*.csv")));
			//BufferedReader br= new BufferedReader(new InputStreamReader(in,"GBK"));//这里如果csv文件编码格式是utf-
			
			bw = new BufferedWriter(osw);
			if (dataList != null && !dataList.isEmpty()) {
				for (String data : dataList) {
					bw.append(data).append("\r");
				}
			}
			isSucess = true;
		} catch (Exception e) {
			isSucess = false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
					bw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (osw != null) {
				try {
					osw.close();
					osw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return isSucess;
	}

	public static List<String> exportCsv(File file) {
		List<String> dataList = new ArrayList<String>();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = br.readLine()) != null) {
				dataList.add(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
					br = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return dataList;
	}

	public static void exportCsv(){
		List<String> dataList=CSVUtil.exportCsv(new File("D:/test/ljq.csv"));
		if(dataList!=null && !dataList.isEmpty()){
			for(int i=0; i<dataList.size();i++ ){
				if(i!=0){//不读取第一行
					String s = dataList.get(i);
					System.out.println("s  "+s);
					String[] as = s.split(",");
					System.out.println(as[0]);
					System.out.println(as[1]);
					System.out.println(as[2]);
				}
			}
		}
	}

	public static void importCsv(List dataList,String path) {
	    boolean isSuccess = CSVUtil.importCsv(new File(path),dataList);
		System.out.println(isSuccess);
		
	}
	
	public static void main(String args[]){
		List dataList = new ArrayList();
		dataList.add("label1^Alabel2^Alabel3^Alabel4^Alabel5");
		dataList.add("哈哈1^A哈哈2^A哈哈3^A哈哈4^A哈哈5");
		dataList.add("嘻嘻1^A嘻嘻2^A嘻嘻3^A嘻嘻4^A嘻嘻5");
		dataList.add("解决1^A解决2^A解决3^A解决4^A解决5");
		
		importCsv(dataList,"C:/workspace/hello.csv");
		
	}
}
