package com.derbysoft.util;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class DataCompUtil {
	
	public static void main(String []arg0){
        String []dateArray = {"2013-04-01","2013-04-08","2013-04-28",
            "2013-04-08", "2013-11-11"};
        showResult(dateArray);
    }

    private static void showResult(String[] dateArray) {
        Map<String, Integer> dateMap = new TreeMap<String, Integer>();
        int i, arrayLen;
        arrayLen = dateArray.length;
        for(i = 0; i < arrayLen; i++){
            String dateKey = dateArray[i];
            if(dateMap.containsKey(dateKey)){
                int value = dateMap.get(dateKey) + 1;
                dateMap.put(dateKey, value);
            }else{
                dateMap.put(dateKey, 1);
            }
        }
        Set<String> keySet = dateMap.keySet();
        String []sorttedArray = new String[keySet.size()];
        Iterator<String> iter = keySet.iterator();
        int index = 0;
        while (iter.hasNext()) {
            String key = iter.next();
        //    System.out.println(key + ":" + dateMap.get(key));
            sorttedArray[index++] = key;
        }
        int sorttedArrayLen = sorttedArray.length;
        System.out.println("最小日期是：" + sorttedArray[0] + "," +
                " 天数为" + dateMap.get(sorttedArray[0]));
        System.out.println("最大日期是：" + sorttedArray[sorttedArrayLen - 1] + "," +
                " 天数为" + dateMap.get(sorttedArray[sorttedArrayLen - 1]));
    }
}
