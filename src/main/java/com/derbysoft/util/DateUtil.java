package com.derbysoft.util;

import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DateUtil {
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	public List getDateList(List list) throws Exception{//根据零散的日期处理成日期是一段段的
		Calendar calendar = Calendar.getInstance();
		
		Date startDate = dateFormat.parse(list.get(0).toString());
		Date endDate = null;
		boolean is = false;
	 	
		List<Map<String,Object>> tempList= new ArrayList<Map<String,Object>>();
		
		for (int i = 0; i < list.size(); i++) {
			if(is){
				startDate = dateFormat.parse(list.get(i).toString());
				is = false;
			}
			Date parse = dateFormat.parse(list.get(i).toString());
			Date parseNext = null;
			if((i+1)==list.size()){
				parseNext = dateFormat.parse(list.get(i).toString());
			}else{
				parseNext = dateFormat.parse(list.get(i+1).toString());
			}
					
			calendar.setTime(parse);
			calendar.add(Calendar.DATE, 1);
			Date time2 = calendar.getTime();
						
			//如果parse+1天不等于parse1
			if (parseNext .compareTo(time2)!=0 || (i+1)==list.size()) {		
				if ((i+1)==list.size()) {		//如果循环到最后一个
					endDate=parseNext;
				}else{
					endDate=parse;
				}
				Map<String,Object> map1 = new HashMap<String,Object>();
				map1.put("begin", dateFormat.format(startDate));
				map1.put("end", dateFormat.format(endDate));
				tempList.add(map1);
				startDate = parseNext ;
				is = true;
			}
		}
		
		return tempList;
	}
	
	// 方式2: 升序排列
    public List<String> search(List<String> list){
       Collections.sort(list, new Comparator<String>() {
           @Override
           public int compare(String o1, String o2) {
        	   Date date1 = null;
        	   Date date2 = null;
        	   try {
        		   date1 = dateFormat.parse(o1);
        		   date2 = dateFormat.parse(o2);
				
        		   if ((date1.getTime() > date2.getTime())){
	                   return 1;
        		   }
        		   if (date1.getTime() == date2.getTime()){
	                   return 0;
        		   }
	        
        	   } catch (ParseException e) {
        		   // TODO Auto-generated catch block
        		   e.printStackTrace();
        	   }
        	   return -1;
           	 }
           });
       return list;
   }
	
    
    /**
     * 计算两个日期的天数差
     * @param d1
     * @param d2
     * @return
     */
    public static int getTianshu(Date d1, Date d2){
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        //long类型的日期也支持
//        cal1.setTimeInMillis(long);
//        cal2.setTimeInMillis(long);
        cal1.setTime(d1);
        cal2.setTime(d2);

        //获取日期在一年(月、星期)中的第多少天
        int day1 = cal1.get(Calendar.DAY_OF_YEAR);//第335天
        int day2 = cal2.get(Calendar.DAY_OF_YEAR);//第365天

        //获取当前日期所在的年份
        int year1 = cal1.get(Calendar.YEAR);
        int year2 = cal2.get(Calendar.YEAR);

        //如果两个日期的是在同一年，则只需要计算两个日期在一年的天数差；
        //不在同一年，还要加上相差年数对应的天数，闰年有366天

        if(year1 != year2) //不同年
        {
            int timeDistance = 0 ;
            for(int i = year1 ; i < year2 ; i ++)
            {
                if(i%4==0 && i%100!=0 || i%400==0) //闰年
                {
                    timeDistance += 366;
                }
                else //不是闰年
                {
                    timeDistance += 365;
                }
            }
            System.out.println(timeDistance + (day2-day1));
            return timeDistance + (day2-day1);
        }
        else //同年
        {
            System.out.println("判断day2 - day1 : " + (day2-day1));
            return day2-day1;
        }
    }
	
	public static void main(String args[]) throws Exception{
		DateUtil util = new DateUtil();
		List alist = new ArrayList();
		
		alist.add("2020-01-01");
		
		alist.add("2020-01-05");
		alist.add("2020-01-04");
		alist.add("2020-01-07");
		alist.add("2020-01-10");
		
		alist.add("2020-01-17");
		alist.add("2020-01-16");
		alist.add("2020-01-15");
		alist.add("2020-01-14");
		
		alist = util.search(alist);
		
		List list = util.getDateList(alist);
		
		System.out.println(list);
		
	}
	

}
