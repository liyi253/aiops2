package com.derbysoft.util;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


public class JdbcUtil {
	
	public void process_cancel() throws SQLException {//处理取消订单
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat simple1 = new SimpleDateFormat("yyyyMM");
		
		String URL = "jdbc:mysql://drc-read-s2.cfaojktddrvh.ap-northeast-1.rds.amazonaws.com:3306/drc?characterEncoding=utf-8";
		String USER = "lee.li";
		String PASSWORD = "3zuVM_2Kd*Ugbphj7yQ";
		
		String localURL = "jdbc:mysql://127.0.0.1:3306/aiops?characterEncoding=utf-8";
		String localUSER = "root";
		String localPASSWORD = "root";
		Connection conn = null;
		ResultSet rs = null;
		Statement statement =null;
		
		Connection localconn = null;
		ResultSet localrs = null;
		Statement localstatement =null;
		// 1.加载驱动程序
		try {
			boolean result =true;
			Class.forName("com.mysql.jdbc.Driver");
			
			conn = DriverManager.getConnection(URL, USER, PASSWORD);
			
			/*
			DbConnect dDbConnect = new DbConnect();
			
			try {
				conn = dDbConnect.getConnect("MDB");
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			
			
			System.out.print("链接数据库成功！！！");
			localconn = DriverManager.getConnection(localURL, localUSER, localPASSWORD);
			
			String [] dates ={"201903","201904","201905","201906","201907","201908","201909","201910","201911","201912","202001","202002",
							  "201803","201804","201805","201806","201807","201808","201809","201810","201811","201812","201901","201902",
							  "201703","201704","201705","201706","201707","201708","201709","201710","201711","201712","201801","201802"};
			
			for(int i=0; i<dates.length; i++){
					String sql="select t1.cancel_time_2 as cancel_time,t1.reservation_state,'国内' as countryType,"+
							" count(*) as count from (select t.reservation_state,hotel_code,date_format(cancel_time,'%Y%m%d') as cancel_time_2 "+  
							" from reservation_"+dates[i]+" t where reservation_state ='CANCELLED')t1,"+
							" (select resv_hotel_code from hotel where country_code ='CN') t2"+ 
							" where  t1.hotel_code = t2.resv_hotel_code  group by t1.cancel_time_2,t1.reservation_state"; //国内的
							
					statement = conn.createStatement();			
					rs = statement.executeQuery(sql);
					System.out.print(i+"执行查询sql语句成功！！！");
					
					// 4.处理数据库的返回结果(使用ResultSet类)
					//
					while (rs.next()) {
						
						String cancel_time = rs.getString("cancel_time");
						String reservation_state = rs.getString("reservation_state");
						String countryType = rs.getString("countryType");
						String count = rs.getString("count");
						
						String addsql ="insert into subject (createYearMonth,cancel_time,reservation_state,countryType,count,type) values('"+dates[i]+"','"+cancel_time+"','"+reservation_state+"','"+countryType+"','"+count+"','1')";;
						
						System.out.print("执行新增的sql语句成功！！！");
						
						localstatement = localconn.createStatement();			
						localstatement.executeUpdate(addsql);
						
					}
					
					
					String sql2="select t1.cancel_time_2 as cancel_time,t1.reservation_state,'国外' as countryType,"+
							" count(*) as count from (select t.reservation_state,hotel_code,date_format(cancel_time,'%Y%m%d') as cancel_time_2 "+  
							" from reservation_"+dates[i]+" t where reservation_state ='CANCELLED')t1,"+
							" (select resv_hotel_code from hotel where country_code !='CN') t2"+ 
							" where  t1.hotel_code = t2.resv_hotel_code  group by t1.cancel_time_2,t1.reservation_state"; //国内的
							
					statement = conn.createStatement();			
					rs = statement.executeQuery(sql2);
					
					// 4.处理数据库的返回结果(使用ResultSet类)
					//
					while (rs.next()) {
						String cancel_time = rs.getString("cancel_time");
						String reservation_state = rs.getString("reservation_state");
						String countryType = rs.getString("countryType");
						String count = rs.getString("count");
						
						
						String addsql ="insert into subject (createYearMonth,cancel_time,reservation_state,countryType,count,type) values('"+dates[i]+"','"+cancel_time+"','"+reservation_state+"','"+countryType+"','"+count+"','1')";;

						localstatement = localconn.createStatement();			
						localstatement.executeUpdate(addsql);
						
					}

					
			}
			
		} catch (Exception e) {			
			e.printStackTrace();
		}finally{
			// 关闭资源
					if(conn!=null){
						conn.close();
					}
					if(rs!=null){
						rs.close();
					}
					if(statement!=null){
						statement.close();
					}
					if(localconn!=null){
						localconn.close();
					}
					
					if(localrs!=null){
						localrs.close();
					}
					
					if(localstatement!=null){
						localstatement.close();
					}
		}
	}
	
	public static void conn() throws SQLException {
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM");
		SimpleDateFormat simple1 = new SimpleDateFormat("yyyyMM");
		
		//String URL = "jdbc:mysql://drc-read-s2.cfaojktddrvh.ap-northeast-1.rds.amazonaws.com:3306/drc?characterEncoding=utf-8";
		//String USER = "lee.li";
		//String PASSWORD = "3zuVM_2Kd*Ugbphj7yQ";
		Connection conn = null;
		ResultSet rs = null;
		Statement statement =null;
		// 1.加载驱动程序
		try {
			//Class.forName("com.mysql.jdbc.Driver");
			//conn = DriverManager.getConnection(URL, USER, PASSWORD);
			conn = MyDBManger.getDBManger().getconnection();
			
			String [] dates ={"2020-01","2019-02","2019-01","2018-02","2018-01","2019-12","2019-11","2019-10","2020-02",  "2019-01"};
			//String [] queryCondition ={"country_code ='CN'","country_code !='CN' or country_code is null"};
			String [] queryCondition2 ={" (reservation_state ='CONFIRMED' or reservation_state ='MODIFIED') "," reservation_state ='CANCELLED' "};
			
			for(int i=0; i<dates.length; i++){
				String date_yyyy_MM = dates[i];
				Calendar  cal = Calendar.getInstance();
				
				try {
					cal.setTime(simple.parse(date_yyyy_MM));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				for(int j=1; j<12; j++){
					
					
					String date_yyyyMM = simple1.format(cal.getTime());
					
						for(int p=0; p<queryCondition2.length; p++){
							
							//String sql="select count(*) as sum from (select hotel_code from reservation_"+date_yyyyMM+"  where substr(check_in,1,7)='"+date_yyyy_MM+"'"+ 
							//		" and cancel_time "+queryCondition2[p]+" ) t1, (select resv_hotel_code from hotel where country_code ='CN' ) t2 "+ 
							//		" where  t1.hotel_code = t2.resv_hotel_code "; //国内的

							String sql="select count(*) as sum from (select * from reservation_"+date_yyyyMM+"  where date_format(check_in,'%Y%m%d')>='20190201' and  date_format(check_in,'%Y%m%d')<='20190215'"+ 
											" and  "+queryCondition2[p]+" ) t1, (select resv_hotel_code from hotel where country_code ='CN' ) t2 "+ 
											" where  t1.hotel_code = t2.resv_hotel_code "; //国内的
							
							statement = conn.createStatement();			
							rs = statement.executeQuery(sql);
							// 4.处理数据库的返回结果(使用ResultSet类)
							 //
							while (rs.next()) {
								if(p==0){
									System.out.println(date_yyyy_MM+"--国内的--"+date_yyyyMM+"--被取消的--"+rs.getString("sum")+"\t");
								}else{
									System.out.println(date_yyyy_MM+"--国内的--"+date_yyyyMM+"--没有取消的--"+rs.getString("sum")+"\t");
								}
							}
							
							//String sql2="select count(*) as sum from (select hotel_code from reservation_"+date_yyyyMM+"  where substr(check_in,1,7)='"+date_yyyy_MM+"'"+ 
							//		" and cancel_time "+queryCondition2[p]+" ) t1 "; //全国的
							
							String sql2="select count(*) as sum from (select * from reservation_"+date_yyyyMM+"  where date_format(check_in,'%Y%m%d')>='20190201' and  date_format(check_in,'%Y%m%d')<='20190215'"+ 
									" and  "+queryCondition2[p]+" ) t1, (select resv_hotel_code from hotel where country_code !='CN' ) t2 "+ 
									" where  t1.hotel_code = t2.resv_hotel_code "; //国外的

							statement = conn.createStatement();			
							rs = statement.executeQuery(sql2);
							// 4.处理数据库的返回结果(使用ResultSet类)
							 //
							while (rs.next()) {
								if(p==0){
									System.out.println(date_yyyy_MM+"--国外的--"+date_yyyyMM+"--被取消的--"+rs.getString("sum")+"\t");
								}else{
									System.out.println(date_yyyy_MM+"--国外的--"+date_yyyyMM+"--没有取消的--"+rs.getString("sum")+"\t");
								}
								
								
							}

						}
					
					cal.add(cal.MONTH, -1);
				}
			}
			
		} catch (Exception e) {			
			e.printStackTrace();
		}finally{
		// 关闭资源
					conn.close();
					rs.close();
					statement.close();
					
		}
	}
	
	public static void main(String args[]){
		try {
			//conn();
			JdbcUtil jdbc = new JdbcUtil();
			jdbc.process_cancel();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
