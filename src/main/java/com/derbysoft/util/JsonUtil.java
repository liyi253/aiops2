package com.derbysoft.util;


import java.io.BufferedReader;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.alibaba.fastjson.JSONArray;
import com.derbysoft.priceforcast.model.Hotelnfo;

import net.sf.json.JSONObject;


public class JsonUtil {
	
	public static void main(String args[]){
		
		//String info = getInfo("HILTON","NYCNH","OTA");
		String info =getHotelInfoBySupplier("http://dmx.derbysoftsec.com/dmx/warrior/dstorage-build/hotel/json?page=1&pageSize=20&provider=MARRIOTT&hotel=");
		
		List<Hotelnfo> list = JSONArray.parseArray(info, Hotelnfo.class);
		
		if(list!=null){
			for(int i=0; i<list.size(); i++){
				Hotelnfo hotelInfo = list.get(i);
				String hotelCode = hotelInfo.getCode();//根据hotelCode去获取相关的价格信息
			}
		}
		
		System.out.println(info);
	}
	
	public static String getLosRateByCondition(String provider,String hotel,String datetime) throws Exception{
		String allurl = "http://dmx.derbysoftsec.com/dmx/warrior/shop-storage-build-us/get_los_ari/json?page=1&pageSize=20&channel=OTA&provider="+provider+"&hotel="+hotel+"&checkIn="+datetime+"&ratePlan=&roomType=&los=";			
		String result = "";
        
		
            URL url = new URL(allurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            // 腾讯地图使用GET
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(12000);
            conn.setReadTimeout(12000);
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
            String line;
            // 获取地址解析结果
            while ((line = in.readLine()) != null) {
                result += line + "\n";
            }
            in.close();
        

        // 转JSON格式
        
        return result;
	
	}
	
	
	public static String getInfo(String provider,String hotel,String channel){
		String allurl ="http://dmx.derbysoftsec.com/dmx/warrior/dstorage-build/roomrate/json?page=1&pageSize=20&provider="+provider+"&hotel="+hotel+"&channel="+channel;
			
		  String result = "";
	        try {
	            URL url = new URL(allurl);
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            // 腾讯地图使用GET
	            conn.setRequestMethod("GET");
	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	            String line;
	            // 获取地址解析结果
	            while ((line = in.readLine()) != null) {
	                result += line + "\n";
	            }
	            in.close();
	        } catch (Exception e) {
	            e.getMessage();
	        }

	        // 转JSON格式
	        
	        return result;
	}
	
	public static String getCmdbInfoByUrl(String url2,String authorization) throws Exception{
		
	  	String result ="";

        URL url = new URL(url2);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        // 腾讯地图使用GET
            
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", authorization);
        conn.setDoOutput(true);
        conn.setDoInput(true);
        //conn.setRequestProperty("connection", "keep-alive");
        conn.setConnectTimeout(30000);
        conn.setReadTimeout(30000);
        conn.setUseCaches(false);
        conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            
        BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
        String line;
        // 获取地址解析结果
        while ((line = in.readLine()) != null) {
            result += line + "\n";
        }
        in.close();
        
        String data  = JSONObject.fromObject(result).getString("data");//获取酒店信息
        
        return data;
	}
	
	public static String getHotelInfoBySupplier(String url2){
			
		  	String result ="";
		  	
		  	try {
	            URL url = new URL(url2);
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            // 腾讯地图使用GET
	            conn.setRequestMethod("GET");
	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	            String line;
	            // 获取地址解析结果
	            while ((line = in.readLine()) != null) {
	                result += line + "\n";
	            }
	            in.close();
	        } catch (Exception e) {
	            e.getMessage();
	        }
	        
	        String value  = JSONObject.fromObject(result).getString("value");//获取酒店信息
	        
	        

	        // 转JSON格式
	        return value;
	}
	
	
	public static String getCpuMemInfo(String startTime,String endTime){
		String range = startTime+"-"+endTime;

		String allurl ="http://10.110.233.192:10011/get_metrics/"+range;

		  String result = "";
	        try {
	            URL url = new URL(allurl);
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	            conn.setDoOutput(true);
	            // 腾讯地图使用GET
	            conn.setRequestMethod("GET");
	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
	            String line;
	            // 获取地址解析结果
	            while ((line = in.readLine()) != null) {
	                result += line + "\n";
	            }
	            in.close();
	        } catch (Exception e) {
	            e.getMessage();
	        }

	        // 转JSON格式
	        
	 
	        if(result!=null){
	        	try {
					String cpu  = JSONObject.fromObject(result).getString("cpu");
					String memory  = JSONObject.fromObject(result).getString("memory");
					String socket  = JSONObject.fromObject(result).getString("socket");
					
					String cpu_max  = JSONObject.fromObject(cpu).getString("max");
					String cpu_mean  = JSONObject.fromObject(cpu).getString("mean");
					String cpu_median  = JSONObject.fromObject(cpu).getString("median");
					String cpu_min  = JSONObject.fromObject(cpu).getString("min");
					String cpu_std  = JSONObject.fromObject(cpu).getString("std");
					String cpu_var  = JSONObject.fromObject(cpu).getString("var");
					
					
					String memory_max  = JSONObject.fromObject(memory).getString("max");
					String memory_mean  = JSONObject.fromObject(memory).getString("mean");
					String memory_median  = JSONObject.fromObject(memory).getString("median");
					String memory_min  = JSONObject.fromObject(memory).getString("min");
					String memory_std  = JSONObject.fromObject(memory).getString("std");
					String memory_var  = JSONObject.fromObject(memory).getString("var");
					
					String result2 = cpu_max+","+cpu_mean+","+cpu_median+","+cpu_min+","+cpu_std+","+cpu_var+","
						  +memory_max+","+memory_mean+","+memory_median+","+memory_min+","+memory_std+","+memory_var;	
					
					return result2;
					
				} catch (Exception e) {
					e.printStackTrace();
				}
	       }
	        
	        return null;

	}
	
	
	

}
