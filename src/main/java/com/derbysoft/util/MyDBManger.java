package com.derbysoft.util;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.sql.Connection;
import java.sql.SQLException;
public class MyDBManger {
/**
 * @author sunyang_ah@163.com
 * @return 
 * @function  利用数据库连接池对数据库进行连接
 * */
	ComboPooledDataSource combo = new ComboPooledDataSource();
	
	private MyDBManger() {
		// TODO Auto-generated method stub
		try {
			combo.setDriverClass("com.mysql.jdbc.Driver"); // 加载驱动
			combo.setPassword("3zuVM_2Kd*Ugbphj7yQ"); 
			combo.setUser("lee.li");
			combo.setJdbcUrl("jdbc:mysql://drc-read-s2.cfaojktddrvh.ap-northeast-1.rds.amazonaws.com:3306/drc?characterEncoding=utf-8"); // 地址可以换成云端
			combo.setMaxPoolSize(10); //池中最大的数量
			combo.setMinPoolSize(3);
			combo.setCheckoutTimeout(600000); // 超时连接，断
			//测试下
			combo.getConnection().close();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("error : "+ e.getMessage());
		}
		
	}
	
	private static MyDBManger DBManager = new MyDBManger();
	public static MyDBManger getDBManger(){
		return DBManager;
	}	
	//外面调用获得数据库连接对象，调用此方法
	public Connection getconnection() throws SQLException{
		return combo.getConnection();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new MyDBManger();
	}
 
}