package com.derbysoft.util;


import redis.clients.jedis.Jedis;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.ShardedJedis;
import redis.clients.jedis.ShardedJedisPool;


public class RedisClient {
	
	private Jedis jedis;//非切片额客户端连接
    private JedisPool jedisPool;//非切片连接池
    private ShardedJedis shardedJedis;//切片额客户端连接
    private ShardedJedisPool shardedJedisPool;//切片连接池
    
    public RedisClient() 
    { 
        initialPool(); 
        jedis = jedisPool.getResource(); 
    } 
 
    private void initialPool() 
    { 
        // 池基本配置 
        JedisPoolConfig config = new JedisPoolConfig(); 
        config.setMaxWaitMillis(1000l); 
        config.setMaxIdle(5); 
        config.setMaxTotal(20);
        config.setTestOnBorrow(false); 
        
        jedisPool = new JedisPool(config,"10.110.233.192",6379);
    }
    
    public String get(String key){
    	String countstr = jedis.get(key);
    	
    	return countstr;
    }
    
    public String set(String key,String value){
    	String countstr = jedis.set(key, value);
    	jedis.expire(key, 120);
    	return countstr;
    }
    
    public static void main(String args[]){
    	RedisClient redisClient = new RedisClient();
    	String str = redisClient.get("2019-12-10 08:13:30");
    	System.out.println(str);
    }

}
