package com.derbysoft.util;

import java.io.BufferedReader;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;


public class S3Util {
	
    String bucketName = "bps-uat-prometheus4aiops"; // 【你 bucket 的名字】 # 首先需要保证 s3 上已经存在该存储桶
    
    SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
    
    public static void createFolder(String bucketName, String folderName, AmazonS3 client) {
        // create meta-data for your folder and set content-length to 0
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(0);
        // create empty content
        InputStream emptyContent = new ByteArrayInputStream(new byte[0]);
        // create a PutObjectRequest passing the folder name suffixed by /
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName,
                    folderName + "/", emptyContent, metadata);
        // send request to S3 to create folder
        client.putObject(putObjectRequest);
    }
    
    public String downloadS3(String hotelcode,String time) throws IOException {
        try {
        	AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();
        	
            System.out.println("Downloading an object");
        	S3Object s3object = s3.getObject(new GetObjectRequest(bucketName, "/priceforcast3/MARRIOTT/"+hotelcode+"/"+time+"/data.csv"));
        	displayTextInputStream(s3object.getObjectContent());
        			
        	return "ok";
        } catch (AmazonServiceException ase) {
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
        }
        return null;
    }
    
    
    public String uploadToS3(File tempFile, String remoteFileName) throws IOException {
        try {
        	AmazonS3 s3 = AmazonS3ClientBuilder.defaultClient();
        	
        	int index = remoteFileName.lastIndexOf("/");
        	String folderpath = remoteFileName.substring(0, index);
        	createFolder(bucketName,folderpath,s3);
        	
            s3.putObject(new PutObjectRequest(bucketName, remoteFileName, tempFile));
            System.out.println("-------------------");
            
            return "ok";
        } catch (AmazonServiceException ase) {
            ase.printStackTrace();
        } catch (AmazonClientException ace) {
            ace.printStackTrace();
        }
        return null;
    }
    
    public void test(){
        File uploadFile = new File("c:/test.txt");
        String uploadKey = "test";
        try {
			uploadToS3(uploadFile,uploadKey);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private static void displayTextInputStream(InputStream input) throws IOException
    {
      BufferedReader reader = new BufferedReader(new InputStreamReader(input));
      int i =0; 
      while(true)
      {
    	if(i<=1){
    		String line = reader.readLine();
            if(line == null) break;
            System.out.println( "    " + line );
    	}else{
    		break;
    	}
        
        i++;
      }
      System.out.println();
    }
}

