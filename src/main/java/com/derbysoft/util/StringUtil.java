package com.derbysoft.util;

public class StringUtil {
	
	public static String getString(String str){
		
		if(str!=null && str.endsWith(".0")){
			str = str.substring(0, str.length()-2);
		}
		
		return str;
	}
	
	public static void main(String args[]){
		String x = getString("129389.0");
		System.out.println(x);
	}

}
