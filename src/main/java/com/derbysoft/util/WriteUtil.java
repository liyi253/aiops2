package com.derbysoft.util;

import java.io.BufferedReader;
import java.io.File;  
import java.io.FileInputStream;  
import java.io.FileNotFoundException;  
import java.io.FileOutputStream;  
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.RandomAccessFile;

public class WriteUtil {
	
	
    public static void main(String args[]){
    	
    	for(int i=0 ;i<=10;i++){
    		writeTxt("hehhehe\n","C:\\workspace\\importData\\hello.txt");
    	}
    	
    }
	 
	/**
	* 将内容追加到文件尾部
	* A方法追加文件：使用RandomAccessFile
	* @param fileName 文件名
	* @param content 追加的内容
	*/
	public static void writeTxt(String content,String fileName){
		try {
			// 打开一个随机访问文件流，按读写方式
			//RandomAccessFile randomFile = new RandomAccessFile("\\home\\xQWoLyOLxMbtajSNM4Z\\tjxx.txt", "rw");
			RandomAccessFile randomFile = new RandomAccessFile("C:\\workspace\\importData\\"+fileName, "rw");
			
			// 文件长度，字节数
			long fileLength = randomFile.length();
			//将写文件指针移到文件尾。
			randomFile.seek(fileLength);
			randomFile.writeBytes(content);
			randomFile.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	
	/**
	* 将内容追加到文件尾部
	* A方法追加文件：使用RandomAccessFile
	* @param fileName 文件名
	* @param content 追加的内容
	*/
	public static void writeCSV(String content,String fileName){
		try {
			// 打开一个随机访问文件流，按读写方式
			//RandomAccessFile randomFile = new RandomAccessFile("\\home\\xQWoLyOLxMbtajSNM4Z\\tjxx.txt", "rw");
			RandomAccessFile randomFile = new RandomAccessFile("C:\\workspace\\importData\\"+fileName, "rw");
			
			// 文件长度，字节数
			long fileLength = randomFile.length();
			//将写文件指针移到文件尾。
			randomFile.seek(fileLength);
			randomFile.writeBytes(content);
			randomFile.close();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	/**
	* 将内容追加到文件尾部
	* A方法追加文件：使用RandomAccessFile
	* @param fileName 文件名
	* @param content 追加的内容
	*/
	public static void writeOne(String content,String fileName){
		
		byte[] b = content.getBytes();  //将字符串转换成字节数
        OutputStream out = null;//![](https://img2018.cnblogs.com/blog/1480523/201910/1480523-20191016181410606-1043685942.png)


        try {
            out = new FileOutputStream("C:\\workspace\\importData\\"+fileName);  //实例化OutpurStream
            out.write(b);    //写入
        }catch(Exception e){
            e.printStackTrace();
        } finally {
            if (out != null){
                try {
                    out.close();    //关闭
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
		
		/*
		try {
			// 打开一个随机访问文件流，按读写方式
			//RandomAccessFile randomFile = new RandomAccessFile("\\home\\xQWoLyOLxMbtajSNM4Z\\tjxx.txt", "rw");
			RandomAccessFile randomFile = new RandomAccessFile("C:\\workspace\\importData\\"+fileName, "rw");
			
			// 文件长度，字节数
			long fileLength = randomFile.length();
			//将写文件指针移到文件尾。
			randomFile.seek(fileLength);
			randomFile.writeBytes(content);
			randomFile.close();
		} catch (IOException e){
			e.printStackTrace();
		}*/
	}
	
}
