package com.derbysoft.volumeforcast;

import java.util.ArrayList;



import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicLong;

import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.storage.shop.save.dto.DailyAris;
import com.derbysoft.storage.shop.save.dto.DailyRates;
import com.derbysoft.storage.shop.save.dto.LosInventory;
import com.derbysoft.storage.shop.save.dto.OccupancyDailyRate;
import com.derbysoft.storage.shop.save.dto.SaveDailyAriRQ;
import com.derbysoft.storage.shop.save.remote.DefaultSaveAriRemoteService;
import com.derbysoft.storage.shop.save.remote.SaveDailyAriRequest;
import com.derbysoft.storage.shop.save.remote.SaveDailyAriResponse;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;

import javax.annotation.PostConstruct;


public class TestLos {

	private static CountDownLatch cd = new CountDownLatch(1);

	public static AtomicLong requestResponseNum = new AtomicLong(0);
	
	public static AtomicLong requestNum = new AtomicLong(0);
		
	public static long allbeginTime = 0;
	
	public static String allbeginTime_2 = "";//这个是以"yyyy-MM-dd HH:mm:ss"
	
	public static long allendTime = 0;
	
	public static String allendTime_2 = "";//这个是以"yyyy-MM-dd HH:mm:ss"
	
	public static AtomicLong exitNum = new AtomicLong(0);
	
	public static int CONCURRENCE_COUNT = 200;//线程数量
	
	public static String hotelcode;//酒店代码
	
	public static String los;      //测试天数 
	
	public static String duringtime;//持续时间(秒数)
	
	public static long sleeptime;//每个请求结束之后的休息时间
	
	public static List respTimeList = new ArrayList();
	
	public static ConcurrentHashMap timemap = new ConcurrentHashMap();
	
	public static boolean lock = false;
	
	public static DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService = new DefaultHotelBuyerRemoteService("10.110.206.140:9002");
	
	@PostConstruct
	public static void main(String args[]) {
		
		try {
			 
			 if(args[0]==null|| args[0]==""){
				 System.out.println("请输入第一个参数线程数量!");
				 return;
			 }
			 
			 if(args[1]==null||args[1]==""){
				 System.out.println("请输入第二个参数hotelcode!");
				 return;
			 }
			 
			 if(args[2]==null||args[2]==""){
				 System.out.println("请输入第三个参数los!");
				 return;
			 }
			 
			 if(args[3]==null||args[3]==""){
				 System.out.println("请输入第四个参数持续时间(s)");
				 return;
			 } 
			 
			 if(args[4]==null||args[4]==""){
				 System.out.println("休息时间");//是为了减缓发送频率
				 return;
			 }
			  
			 CONCURRENCE_COUNT = Integer.parseInt(String.valueOf(args[0]));//线程数量
			 hotelcode = String.valueOf(args[1]);//hotelcode
			 los = String.valueOf(args[2]);//los
			 duringtime = String.valueOf(args[3]);//持续时间
			 sleeptime = Long.parseLong(String.valueOf(args[4]));//每个请求的休息时间
			 
			 /*
			 CONCURRENCE_COUNT = 1;//线程数量
			 hotelcode = "L400-200";//hotelcode
			 los = "4";//los
			 duringtime = "360";//持续时间
			 sleeptime =0;
             */
			 
			for (int i = 0; i < CONCURRENCE_COUNT; i++) {
				 
				new Thread(new SendTask()).start();
			}
				 
			long currentTimeMillis = System.currentTimeMillis();
				 
			System.out.println("当前用时：" + (System.currentTimeMillis() - currentTimeMillis));
			 
			cd.countDown();
				
		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
	}

	/**
	 * 线程类
	 */
	private static class SendTask implements Runnable {
		@Override
		public void run() {
		 
		try {
			System.out.println("线程进入等待");
			cd.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
			TestQueryLos.query(defaultHotelBuyerRemoteService);
		}
	}

}