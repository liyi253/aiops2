package com.derbysoft.volumeforcast;


import java.text.ParseException;




import java.text.SimpleDateFormat;
import java.util.ArrayList;


import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.tomcat.jni.Time;
import com.derbysoft.dswitch.dto.hotel.avail.HotelAvailCriteriaDTO;
import com.derbysoft.dswitch.dto.hotel.avail.HotelAvailRQ;
import com.derbysoft.dswitch.dto.hotel.avail.RoomStayCandidateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeDTO;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRQ;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateChangeRS;
import com.derbysoft.dswitch.dto.hotel.cds.LOSRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.OccupancyRateDTO;
import com.derbysoft.dswitch.dto.hotel.cds.RatesDTO;
import com.derbysoft.dswitch.dto.hotel.common.DateRangeDTO;
import com.derbysoft.dswitch.dto.hotel.common.FeesDTO;
import com.derbysoft.dswitch.dto.hotel.common.StayDateRangeDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelAvailRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelAvailResponse;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelLOSRateChangeResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.storage.remote.provider.StorageProviderRemoteService;

import com.derbysoft.util.JsonUtil;
import com.derbysoft.util.RedisClient;
import com.derbysoft.util.WriteUtil;



public class TestQuery2 {
	
    public static HotelAvailResponse  query(DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService)  {
		
    	defaultHotelBuyerRemoteService.setConnectionSize(2000);
    	
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
		
		SimpleDateFormat simple2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
    	HotelAvailRequest request = new HotelAvailRequest();
		request.setHeader(new RequestHeader("hhfd", "DAILY", "DDFF"));
		
		HotelAvailRQ hotelAvailRQ =new HotelAvailRQ(); 
		HotelAvailCriteriaDTO dato = new HotelAvailCriteriaDTO();
		
		List<String> hotelCodes = new ArrayList();
		//这里要传入一个酒店code
		hotelCodes.add(Test.hotelcode);
		
		dato.setHotelCodesList(hotelCodes);//只传一个
		
		StayDateRangeDTO stayDateRangeDTO = new StayDateRangeDTO();
		
		stayDateRangeDTO.setCheckin("2019-12-19");
		
		Calendar cal =Calendar.getInstance();
        try {
			cal.setTime(simple.parse("2019-12-19"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        int amount = (Test.los==null?1:Integer.parseInt(Test.los));
        cal.add(cal.DATE, amount);
        
        Date enddate = cal.getTime();

        stayDateRangeDTO.setCheckout(simple.format(enddate));
		dato.setStayDateRange(stayDateRangeDTO);
		RoomStayCandidateDTO room = new RoomStayCandidateDTO();
		
		room.setAdultCount(2);
		room.setChildCount(0);
		room.setNumberOfUnits(1);
		
		dato.setRoomStayCandidate(room);
		hotelAvailRQ.setAvailCriteria(dato);

		request.setHotelAvailRQ(hotelAvailRQ);
		HotelAvailResponse response =null;
		
		StorageProviderRemoteService service =null;
		
	    for(long j=0; j<100000;j++) {
	    	if(j==0){
	    		response = defaultHotelBuyerRemoteService.getAvailability(request);
	    		
	    	}else{
	    		if(j==1){
		    		Test.timemap.put("beginTime"+Thread.currentThread().getId(),String.valueOf(System.currentTimeMillis()));
		    		Test.timemap.put("beginTime_2"+Thread.currentThread().getId(),simple2.format(new Date()));
		    	}
		    	
		    	Test.requestNum.incrementAndGet();//请求数增加1
		    	Test.timemap.put("startTime"+Thread.currentThread().getId(),String.valueOf(System.currentTimeMillis()));;
		    	//用这种方式保存时间就是怕值被其他线程修改
		    	response = defaultHotelBuyerRemoteService.getAvailability(request);
		    	
		    	try {
		    		if(Test.sleeptime!=0){
		    			Thread.currentThread().sleep(Test.sleeptime);
		    		}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    	
			    
		    	long sum = Test.requestResponseNum.incrementAndGet();//总的相应数
		    	
		    	if(sum==1){//第一个线程
		    		Test.allbeginTime = new Long(String.valueOf(Test.timemap.get("beginTime"+Thread.currentThread().getId())));
		    		Test.allbeginTime_2 = String.valueOf(Test.timemap.get("beginTime_2"+Thread.currentThread().getId()));
			    	
		    	}else{
		    		if((Test.lock==true)&&((System.currentTimeMillis()-Test.allbeginTime)/1000>=Long.parseLong(Test.duringtime))){
		    		    
		    		    long cishu = Test.exitNum.incrementAndGet();
		    		    Test.allendTime = System.currentTimeMillis();
		    		    Test.allendTime_2 = simple2.format(new Date());
		    		    
		    		    try {
							Thread.currentThread().sleep(20000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
		    			
		    			break;//当前时间减去所有线程启动开始的时候>=持续时间
		    		}
		    	}
		    	
		    	//System.out.print("-------endTime=---------"+Test.timemap.get("endTime"+Thread.currentThread().getId()));
		    	long  duringTime = System.currentTimeMillis() - new Long(String.valueOf(Test.timemap.get("startTime"+Thread.currentThread().getId())));//相应时间
		    	
		    	Test.respTimeList.add(duringTime);//相应时间列表
		    	
		    	//Test.timemap.put("endTime"+Thread.currentThread().getId(),String.valueOf(System.currentTimeMillis()));
		    	if(((System.currentTimeMillis()-Test.allbeginTime)/1000)>=Long.parseLong(Test.duringtime)){
		    		
		    		if(Test.lock==false){
	    		    	Test.lock=true;
	    		    	//统计QPS:
	    		    	Test.allendTime = System.currentTimeMillis();
	    		    	Test.allendTime_2 = simple2.format(new Date());
	    		    	
	    		    	try {
							Thread.currentThread().sleep(5000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
	    		    	
	    		    	long allDuringTime = Test.allendTime-Test.allbeginTime;
	    		    	String rps = "";//RPS
	    		    	
	    		    	System.out.println("开始初始化redis客户端");
	    		    	
	    		    	RedisClient redisClient = new RedisClient();
	    		    	
	    		    	System.out.println("结束初始化redis客户端");
	    		    	
	    		    	try {
							Date allbeginDate = simple2.parse(Test.allbeginTime_2);
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(allbeginDate);
							
							Date allendDate = simple2.parse(Test.allendTime_2);
							
							long secondInterval = (allendDate.getTime()-allbeginDate.getTime())/1000;
							
							long allsum = 0;
							
							System.out.println("开始处理secondInterval");
							
							for(int k=0 ;k<secondInterval-2; k++){
								if(k==0){
									calendar.add(calendar.SECOND, 2);
								}else{
									calendar.add(calendar.SECOND, 1);
								}
								String time = simple2.format(calendar.getTime());
								
								String count = redisClient.get(time);
								if(count!=null){
									allsum += Long.parseLong(count);
								}
							}
							
							System.out.println("结束处理secondInterval");
							
							rps = String.valueOf(allsum/(secondInterval-2));
						} catch (Exception e1) {
							e1.printStackTrace();
						}
	    		    	
	    		    	
	    		    	String info = JsonUtil.getCpuMemInfo(String.valueOf(Test.allbeginTime/1000),String.valueOf(Test.allendTime/1000));
	    		    	String qps = String.valueOf(sum/(allDuringTime/1000)); //QPS
	    		    	
	    		    	long allsum =0;
	    		    	
	    		    	//System.out.println("-------Test.respTimeList-------"+Test.respTimeList);
	    		    	//System.out.print("-------Test.respTimeList.size()=---------"+Test.respTimeList.size());
	    		    	long errsum =0;
	    		    	
	    		    	for(int k=0 ; k<Test.respTimeList.size() ;k++){
	    		    		
	    		    		if(Test.respTimeList.get(k)==null){
	    		    			allsum += 0;
	    		    			errsum++;
	    		    		}else{
	    		    			allsum += new Long(String.valueOf(Test.respTimeList.get(k)));
	    		    		}
	    		    	}
	    		    	
	    		    	System.out.println("结束处理respTimeList");
	    		    	
	    		    	long avgResTime = allsum/(Test.respTimeList.size()-errsum);//平均相应时间
	    		    	
	    		    	WriteUtil.writeTxt("DailyTest,"+Test.CONCURRENCE_COUNT+","+Test.hotelcode+","+Test.los+","+Test.duringtime+","+rps+","+qps+","+avgResTime+","+info+"\n","dataDaily.txt");
	    		    	
	    		    	System.out.println(avgResTime+","+Test.hotelcode+","+Test.los+","+Test.duringtime+","+rps+","+qps+","+avgResTime+","+info);
	    		    	
	    		    	
	    		    }
		    		break;//当前时间减去所有线程启动开始的时候>=持续时间
	    		}
	    		
	    	}
	    	
	    }
		
		return response;
	}

    /*
	public static void main(String args[]){
		HotelAvailResponse response = query();
		System.out.print(response.getTaskId());
	}*/

}
