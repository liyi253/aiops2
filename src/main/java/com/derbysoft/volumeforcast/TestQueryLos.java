package com.derbysoft.volumeforcast;


import java.text.ParseException;




import java.text.SimpleDateFormat;
import java.util.ArrayList;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.tomcat.jni.Time;
import com.derbysoft.dswitch.dto.hotel.avail.HotelAvailCriteriaDTO;
import com.derbysoft.dswitch.dto.hotel.avail.HotelAvailRQ;
import com.derbysoft.dswitch.dto.hotel.avail.RoomStayCandidateDTO;
import com.derbysoft.dswitch.dto.hotel.common.StayDateRangeDTO;
import com.derbysoft.dswitch.remote.hotel.buyer.DefaultHotelBuyerRemoteService;
import com.derbysoft.dswitch.remote.hotel.dto.HotelAvailRequest;
import com.derbysoft.dswitch.remote.hotel.dto.HotelAvailResponse;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.storage.remote.provider.StorageProviderRemoteService;
import com.derbysoft.util.JsonUtil;
import com.derbysoft.util.RedisClient;
import com.derbysoft.util.WriteUtil;



public class TestQueryLos {
	
    public static HotelAvailResponse  query(DefaultHotelBuyerRemoteService defaultHotelBuyerRemoteService)  {
		
    	defaultHotelBuyerRemoteService.setConnectionSize(2000);
    	
		SimpleDateFormat simple = new SimpleDateFormat("yyyy-MM-dd");
		
		SimpleDateFormat simple2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
    	HotelAvailRequest request = new HotelAvailRequest();
		request.setHeader(new RequestHeader("hhfd", "LOS", "DDFF"));
		
		HotelAvailRQ hotelAvailRQ =new HotelAvailRQ(); 
		HotelAvailCriteriaDTO dato = new HotelAvailCriteriaDTO();
		
		List<String> hotelCodes = new ArrayList();
		//这里要传入一个酒店code
		hotelCodes.add(TestLos.hotelcode);
		
		dato.setHotelCodesList(hotelCodes);//只传一个
		
		StayDateRangeDTO stayDateRangeDTO = new StayDateRangeDTO();
		
		stayDateRangeDTO.setCheckin("2019-12-19");
		
		Calendar cal =Calendar.getInstance();
        try {
			cal.setTime(simple.parse("2019-12-19"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        int amount = (TestLos.los==null?1:Integer.parseInt(TestLos.los));
        cal.add(cal.DATE, amount);
        
        Date enddate = cal.getTime();

        stayDateRangeDTO.setCheckout(simple.format(enddate));
		dato.setStayDateRange(stayDateRangeDTO);
		RoomStayCandidateDTO room = new RoomStayCandidateDTO();
		
		room.setAdultCount(2);
		room.setChildCount(0);
		room.setNumberOfUnits(1);
		
		dato.setRoomStayCandidate(room);
		hotelAvailRQ.setAvailCriteria(dato);

		request.setHotelAvailRQ(hotelAvailRQ);
		HotelAvailResponse response =null;
		
		StorageProviderRemoteService service =null;
		
	    for(long j=0; j<100000;j++) {
	    	if(j==0){
	    		response = defaultHotelBuyerRemoteService.getAvailability(request);
		    }else{
	    		if(j==1){
		    		TestLos.timemap.put("beginTime"+Thread.currentThread().getId(),String.valueOf(System.currentTimeMillis()));
		    		TestLos.timemap.put("beginTime_2"+Thread.currentThread().getId(),simple2.format(new Date()));
		    	}
		    	
		    	TestLos.requestNum.incrementAndGet();//请求数增加1
		    	TestLos.timemap.put("startTime"+Thread.currentThread().getId(),String.valueOf(System.currentTimeMillis()));;
		    	//用这种方式保存时间就是怕值被其他线程修改
		    	response = defaultHotelBuyerRemoteService.getAvailability(request);
		    	
		    	try {
		    		if(TestLos.sleeptime!=0){
		    			Thread.currentThread().sleep(TestLos.sleeptime);
		    		}
					
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		    	
			    
		    	long sum = TestLos.requestResponseNum.incrementAndGet();//总的相应数
		    	
		    	if(sum==1){//第一个线程
		    		TestLos.allbeginTime = new Long(String.valueOf(TestLos.timemap.get("beginTime"+Thread.currentThread().getId())));
		    		TestLos.allbeginTime_2 = String.valueOf(TestLos.timemap.get("beginTime_2"+Thread.currentThread().getId()));
			    	
		    	}else{
		    		if((TestLos.lock==true)&&((System.currentTimeMillis()-TestLos.allbeginTime)/1000>=Long.parseLong(TestLos.duringtime))){
		    		    
		    		    long cishu = TestLos.exitNum.incrementAndGet();
		    		    TestLos.allendTime = System.currentTimeMillis();
		    		    TestLos.allendTime_2 = simple2.format(new Date());
		    		    
		    		    try {
							Thread.currentThread().sleep(20000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
		    			
		    			break;//当前时间减去所有线程启动开始的时候>=持续时间
		    		}
		    	}
		    	
		    	//System.out.print("-------endTime=---------"+TestLos.timemap.get("endTime"+Thread.currentThread().getId()));
		    	long  duringTime = System.currentTimeMillis() - new Long(String.valueOf(TestLos.timemap.get("startTime"+Thread.currentThread().getId())));//相应时间
		    	
		    	TestLos.respTimeList.add(duringTime);//相应时间列表
		    	
		    	//TestLos.timemap.put("endTime"+Thread.currentThread().getId(),String.valueOf(System.currentTimeMillis()));
		    	if(((System.currentTimeMillis()-TestLos.allbeginTime)/1000)>=Long.parseLong(TestLos.duringtime)){
		    		
		    		if(TestLos.lock==false){
	    		    	TestLos.lock=true;
	    		    	//统计QPS:
	    		    	TestLos.allendTime = System.currentTimeMillis();
	    		    	TestLos.allendTime_2 = simple2.format(new Date());
	    		    	
	    		    	try {
							Thread.currentThread().sleep(5000);
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
	    		    	
	    		    	long allDuringTime = TestLos.allendTime-TestLos.allbeginTime;
	    		    	String rps = "";//RPS
	    		    	
	    		    	System.out.println("开始初始化redis客户端");
	    		    	
	    		    	RedisClient redisClient = new RedisClient();
	    		    	
	    		    	System.out.println("结束初始化redis客户端");
	    		    	
	    		    	try {
							Date allbeginDate = simple2.parse(TestLos.allbeginTime_2);
							Calendar calendar = Calendar.getInstance();
							calendar.setTime(allbeginDate);
							
							Date allendDate = simple2.parse(TestLos.allendTime_2);
							
							long secondInterval = (allendDate.getTime()-allbeginDate.getTime())/1000;
							
							long allsum = 0;
							
							System.out.println("开始处理secondInterval");
							
							for(int k=0 ;k<secondInterval-2; k++){
								if(k==0){
									calendar.add(calendar.SECOND, 2);
								}else{
									calendar.add(calendar.SECOND, 1);
								}
								
								String time = simple2.format(calendar.getTime());
								
								String count = redisClient.get(time);
								if(count!=null){
									allsum += Long.parseLong(count);
								}
							}
							
							System.out.println("结束处理secondInterval");
							
							rps = String.valueOf(allsum/(secondInterval-2));
						} catch (Exception e1) {
							e1.printStackTrace();
						}
	    		    	
	    		    	
	    		    	String info = JsonUtil.getCpuMemInfo(String.valueOf(TestLos.allbeginTime/1000),String.valueOf(TestLos.allendTime/1000));
	    		    	String qps = String.valueOf(sum/(allDuringTime/1000)); //QPS
	    		    	
	    		    	long allsum =0;
	    		    	
	    		    	//System.out.println("-------TestLos.respTimeList-------"+TestLos.respTimeList);
	    		    	//System.out.print("-------TestLos.respTimeList.size()=---------"+TestLos.respTimeList.size());
	    		    	long errsum =0;
	    		    	
	    		    	for(int k=0 ; k<TestLos.respTimeList.size() ;k++){
	    		    		
	    		    		if(TestLos.respTimeList.get(k)==null){
	    		    			allsum += 0;
	    		    			errsum++;
	    		    		}else{
	    		    			allsum += new Long(String.valueOf(TestLos.respTimeList.get(k)));
	    		    		}
	    		    	}
	    		    	
	    		    	System.out.println("结束处理respTimeList");
	    		    	
	    		    	long avgResTime = allsum/(TestLos.respTimeList.size()-errsum);//平均相应时间
	    		    	
	    		    	WriteUtil.writeTxt("LosTest,"+TestLos.CONCURRENCE_COUNT+","+TestLos.hotelcode+","+TestLos.los+","+TestLos.duringtime+","+rps+","+qps+","+avgResTime+","+info+"\n","dataLos.txt");
	    		    	
	    		    	//redisClient.set("result", avgResTime+","+TestLos.hotelcode+","+TestLos.los+","+TestLos.duringtime+","+rps+","+qps+","+avgResTime+","+info);
	    		    	System.out.println("开始打印结果");
	    		    	
	    		    	System.out.println("los:"+avgResTime+","+TestLos.hotelcode+","+TestLos.los+","+TestLos.duringtime+","+rps+","+qps+","+avgResTime+","+info);
	    		    	
	    		    	
	    		    }
		    		break;//当前时间减去所有线程启动开始的时候>=持续时间
	    		}
	    		
	    	}
	    	
	    }
		
		return response;
	}

    /*
	public static void main(String args[]){
		HotelAvailResponse response = query();
		System.out.print(response.getTaskId());
	}*/

}
