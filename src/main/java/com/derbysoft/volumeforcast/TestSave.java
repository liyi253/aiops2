package com.derbysoft.volumeforcast;

import java.util.ArrayList;
import java.util.List;

import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.storage.shop.save.dto.DailyAris;
import com.derbysoft.storage.shop.save.dto.DailyRates;
import com.derbysoft.storage.shop.save.dto.LosInventory;
import com.derbysoft.storage.shop.save.dto.OccupancyDailyRate;
import com.derbysoft.storage.shop.save.dto.SaveDailyAriRQ;
import com.derbysoft.storage.shop.save.remote.DefaultSaveAriRemoteService;
import com.derbysoft.storage.shop.save.remote.SaveDailyAriRequest;
import com.derbysoft.storage.shop.save.remote.SaveDailyAriResponse;

public class TestSave {
	
	public static void main(String args[]){
		save();
	}
	
	
	public static SaveDailyAriResponse save() {
		DefaultSaveAriRemoteService service = new DefaultSaveAriRemoteService("10.110.206.140:7002");
		
		SaveDailyAriRequest request = new SaveDailyAriRequest();

		RequestHeader requestHeader = new RequestHeader();

		requestHeader.setSource("source");
		requestHeader.setDestination("destination");
		requestHeader.setTaskId("taskId");
		

		request.setHeader(requestHeader);

		SaveDailyAriRQ saveDailyAriRQ = new SaveDailyAriRQ();

		saveDailyAriRQ.setProvider("DAILY");
		saveDailyAriRQ.setHotel("D400-160"); //HS400,HM400
		//saveDailyAriRQ.setChannel("xiecheng");
		
		List dailyArisList = new ArrayList();
		
		for(int m=1; m<=40;m++){
		    for(int n=1;n<=10;n++){
			
			DailyAris dailyAris = new DailyAris();
			dailyAris.setStartDate("2019-12-11");
			dailyAris.setEndDate("2020-01-10");
			
			dailyAris.setRatePlan("RP"+n);
			
			dailyAris.setRoomType("RT"+m);
			
			dailyArisList.add(dailyAris);
			
			DailyRates dailyRates = new DailyRates();
			dailyRates.setCurrency("CNY");
			
			List occupancyRatesList = new ArrayList();
				
				if(m%40<16){
					for(int i=1; i<=3; i++){
				    	OccupancyDailyRate occupancyDailyRate = new OccupancyDailyRate();
						occupancyDailyRate.setAdult(i);
						occupancyDailyRate.setChild(0);
						occupancyDailyRate.setAmountBeforeTax(new Double(230));
						occupancyRatesList.add(occupancyDailyRate);
					
					}
				}
			
				dailyRates.setOccupancyRatesList(occupancyRatesList);
				dailyAris.setRates(dailyRates);

				List losInventoriesList = new ArrayList();
				LosInventory losInventory = new LosInventory();
				losInventory.setStartLos(1);
				losInventory.setEndLos(30);
				losInventory.setInventory(4);

				losInventoriesList.add(losInventory);
				dailyAris.setLosInventoriesList(losInventoriesList);
				//saveDailyAriRQ.setDailyArisList(dailyArisList);
		     }
		 }
		
		saveDailyAriRQ.setDailyArisList(dailyArisList);

		request.setSaveDailyAriRQ(saveDailyAriRQ);
		

		SaveDailyAriResponse response = service.saveDailyAri(request);
		
		System.out.println("----------response---------"+response.getTaskId());
		
		return response;
	}

}
