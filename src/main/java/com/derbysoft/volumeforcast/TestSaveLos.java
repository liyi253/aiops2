package com.derbysoft.volumeforcast;

import java.util.ArrayList;
import java.util.List;

import com.derbysoft.storage.shop.save.dto.*;
import com.derbysoft.dswitch.remote.hotel.dto.RequestHeader;
import com.derbysoft.storage.shop.save.dto.CancelPolicy;
import com.derbysoft.storage.shop.save.dto.DailyAris;
import com.derbysoft.storage.shop.save.dto.DailyRates;
import com.derbysoft.storage.shop.save.dto.LosAri;
import com.derbysoft.storage.shop.save.dto.LosAris;
import com.derbysoft.storage.shop.save.dto.LosInventory;
import com.derbysoft.storage.shop.save.dto.LosRates;
import com.derbysoft.storage.shop.save.dto.OccupancyDailyRate;
import com.derbysoft.storage.shop.save.dto.OccupancyLosRates;
import com.derbysoft.storage.shop.save.dto.Rate;
import com.derbysoft.storage.shop.save.dto.SaveDailyAriRQ;
import com.derbysoft.storage.shop.save.dto.SaveLosAriRQ;
import com.derbysoft.storage.shop.save.remote.DefaultSaveAriRemoteService;
import com.derbysoft.storage.shop.save.remote.SaveDailyAriRequest;
import com.derbysoft.storage.shop.save.remote.SaveDailyAriResponse;
import com.derbysoft.storage.shop.save.remote.SaveLosAriRequest;
import com.derbysoft.storage.shop.save.remote.SaveLosAriResponse;

public class TestSaveLos {
	
	public static void main(String args[]){
		TestSaveLos test = new TestSaveLos();
		test.save();
	}
	
	public static SaveDailyAriResponse save() {
		
		DefaultSaveAriRemoteService service = new DefaultSaveAriRemoteService("10.110.206.140:7002");
		
		SaveLosAriRequest losrequest = new SaveLosAriRequest();
		
		RequestHeader requestHeader = new RequestHeader();

		requestHeader.setSource("source");
		requestHeader.setDestination("destination");
		requestHeader.setTaskId("taskId");
		
		losrequest.setHeader(requestHeader);
		
		//SaveLosAriRQ 
		SaveLosAriRQ saveLosAriRQ = new SaveLosAriRQ();
		
		saveLosAriRQ.setProvider("LOS");
		saveLosAriRQ.setHotel("L400-200"); //L4=50个  L400=400有100个产品 ,LOS400
		//saveLosAriRQ.setChannel("xiecheng");
		
		List<LosAris> losArisList = new ArrayList();
		
		for(int i=1; i<=10; i++){
			for(int j=1; j<=40; j++){
				LosAris losAris = new LosAris();
				
				losAris.setCheckin("2019-12-19");
				losAris.setRatePlan("RP"+i);
				losAris.setRoomType("RT"+j);
				
				CancelPolicy cancelPolicy = new CancelPolicy();
				cancelPolicy.setCode("AD100P_100PNONREFUNDABLE");
				cancelPolicy.setDescription("");
				
				List losAriList = new ArrayList();
				
				for(int k=1; k<=8;k++){
					if(j%40<20){
						LosAri losAri = new LosAri();
						losAri.setCancelPolicy(cancelPolicy);
						losAri.setFees("Tax1:RNT20P");
						losAri.setInventory(4);
						losAri.setLos(k);
						losAri.setMealPlan("RO");
						
						LosRates losRates = new LosRates();
						losRates.setCurrency("GBP");
						
						
						List<OccupancyLosRates> occupancyRatesList = new ArrayList();
						
						for(int g=1; g<=3;g++){
							OccupancyLosRates  occupancyRates = new OccupancyLosRates();
							occupancyRates.setAdult(g);
							occupancyRates.setChild(0);
							
							//Rate rate = new Rate();
							
							//rate.setAmountBeforeTax(250d);
							//rate.setAmountAfterTax(230d);
							
							//occupancyRates.setAverageRate(rate);
							
							List dailyRatesList = new ArrayList();
							for(int q=1;q<=k;q++){
								
								Rate rates = new Rate();
								rates.setAmountAfterTax(250d+q);
								rates.setAmountBeforeTax(230d+q);
								
								dailyRatesList.add(rates);
							}
							
							occupancyRates.setDailyRatesList(dailyRatesList);
							
							occupancyRatesList.add(occupancyRates);
						}
					
					
						
					losRates.setOccupancyRatesList(occupancyRatesList);
					
					losAri.setRates(losRates);
					
					losAriList.add(losAri);
					}
					
				}
					
				losAris.setLosAriList(losAriList);
				
				losArisList.add(losAris);
			}
		}
		
		saveLosAriRQ.setLosArisList(losArisList);
		losrequest.setSaveLosAriRQ(saveLosAriRQ);
		SaveLosAriResponse losresponse = service.saveLosAri(losrequest);
		
		System.out.println("----------response---------"+losresponse.getTaskId());
		System.out.print("error"+losresponse.getError());
		
		return null;
	}

}
